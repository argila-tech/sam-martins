<x-dashboard>
    <x-slot name="title">{{ __('Bem vindo') }}</x-slot>
    <x-slot name="css">
        <style>
            body{
                background: linear-gradient(87deg, #2dce89 0, #2dcecc 100%) !important;
            }
        </style>
    </x-slot>
    <x-slot name="content">
        <div class="container mt--6">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-7">
                    <div class="card bg-secondary shadow border-0">
                        <div class="card-body px-lg-5 py-lg-5">
                            <div class="text-center text-muted mb-4">
                                <img src="{{ asset('assets/img/logo-admin.png') }}" alt="{{ env('APP_NAME') }}" style="width: 242px;">
                            </div>
                            <form role="form" method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }} mb-3">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                        </div>
                                        <x-input class="form-control" placeholder="{{ __('Email') }}" type="email" name="email" value="{{ old('email') }}"/>
                                    </div>
                                    <x-error field="email" class="invalid-feedback" />
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                        </div>
                                        <x-input class="form-control" name="password" placeholder="{{ __('Senha') }}" type="password" required />
                                    </div>
                                    <x-error field="password" class="invalid-feedback" />
                                </div>
                                <div class="custom-control custom-control-alternative custom-checkbox">
                                    <input class="custom-control-input" name="remember" id="customCheckLogin" type="checkbox" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="customCheckLogin">
                                        <span class="text-muted">{{ __('Lembrar Senha') }}</span>
                                    </label>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success my-4">{{ __('Entrar') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    {{-- <div class="row mt-3">
                        <div class="col-6">
                            @if (Route::has('password.request'))
                                <a href="{{ route('password.request') }}" class="text-light">
                                    <small>{{ __('Forgot password?') }}</small>
                                </a>
                            @endif
                        </div>
                        <div class="col-6 text-right">
                            <a href="{{ route('register') }}" class="text-light">
                                <small>{{ __('Create new account') }}</small>
                            </a>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </x-slot>
</x-dashboard>
