<x-layout>
   <x-slot name="title">{{ $item->title }}</x-slot>
    <x-slot name="content">
        <!-- inner banner -->
        <div class="inner-banner">
            <section class="w3l-breadcrumb">
                <div class="container">
                    <h4 class="inner-text-title font-weight-bold text-white mb-sm-3 mb-2">{{ $item->title }}</h4>
                    <ul class="breadcrumbs-custom-path">
                        <li><a href="{{ route('index') }}">Home</a></li>
                        <li class="active"><span class="fa fa-chevron-right mx-2" aria-hidden="true"></span>{{ $item->title }}</li>
                    </ul>
                </div>
            </section>
        </div>
        <!-- //inner banner -->

        <!-- about section -->
        {{-- <section class="about-section py-5">
            <div class="container py-lg-5 pt-3">
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="about-single p-3 d-flex justify-content-between">
                            <div class="about-icon mr-4">
                                <i class="fa fa-headphones" aria-hidden="true"></i>
                            </div>
                            <div class="about-content">
                                <h5 class="mb-3">Any Questions</h5>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit accusa ntium dolor emque
                                    laudan.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="about-single p-3 d-flex justify-content-between">
                            <div class="about-icon mr-4">
                                <i class="fa fa-laptop" aria-hidden="true"></i>
                            </div>
                            <div class="about-content">
                                <h5 class="mb-3">Online Support</h5>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit accusa ntium dolor emque
                                    laudan.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="about-single p-3 d-flex justify-content-between">
                            <div class="about-icon mr-4">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                            </div>
                            <div class="about-content">
                                <h5 class="mb-3">24/7 Call Center</h5>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit accusa ntium dolor emque
                                    laudan.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> --}}
        <!-- //about section -->

        <!-- about-2 section -->
        <section class="w3l-about-2 pb-5 pt-5">
            <div class="container pb-md-5 pb-4">
                <div class="row align-items-center justify-content-between">
                    <div class="col-12 about-2-secs-left pr-lg-5">
                        <h3 class="title-style mb-sm-3 mb-2">{{ $item->title }}</h3>
                        {!! $item->description !!}
                    </div>
                </div>
            </div>
        </section>
        <!-- //about-2 section -->

        <!-- team section -->
        {{-- <section class="w3l-team py-5">
            <div class="container py-md-5 py-4">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="section-heading text-center mb-sm-5 mb-4">
                            <h3 class="title-style mb-2">Meet our lovely team</h3>
                            <p class="lead">
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque,
                                eaque ipsa quae ab illo inventore.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-md-4">
                        <div class="team-block-single">
                            <div class="team-grids">
                                <a href="#team-single">
                                    <img src="assets/images/team1.jpg" class="img-fluid" alt="">
                                    <div class="team-info">
                                        <div class="social-icons-section">
                                            <a class="fac" href="#facebook">
                                                <span class="fa fa-facebook"></span>
                                            </a>
                                            <a class="twitter mx-2" href="#twitter">
                                                <span class="fa fa-twitter"></span>
                                            </a>
                                            <a class="google" href="#google-plus">
                                                <span class="fa fa-google-plus"></span>
                                            </a>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="team-bottom-block p-4">
                                <h5 class="member mb-1"><a href="#team">Olive Yew</a></h5>
                                <small>CEO of Company</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 mt-md-0 mt-5">
                        <div class="team-block-single active">
                            <div class="team-grids active">
                                <a href="#team-single">
                                    <img src="assets/images/team2.jpg" class="img-fluid" alt="">
                                    <div class="team-info">
                                        <div class="social-icons-section">
                                            <a class="fac" href="#facebook">
                                                <span class="fa fa-facebook"></span>
                                            </a>
                                            <a class="twitter mx-2" href="#twitter">
                                                <span class="fa fa-twitter"></span>
                                            </a>
                                            <a class="google" href="#google-plus">
                                                <span class="fa fa-google-plus"></span>
                                            </a>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="team-bottom-block p-4">
                                <h5 class="member mb-1 active"><a href="#team">Aida Joe</a></h5>
                                <small>Team Leader</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 mt-md-0 mt-5">
                        <div class="team-block-single">
                            <div class="team-grids">
                                <a href="#team-single">
                                    <img src="assets/images/team3.jpg" class="img-fluid" alt="">
                                    <div class="team-info">
                                        <div class="social-icons-section">
                                            <a class="fac" href="#facebook">
                                                <span class="fa fa-facebook"></span>
                                            </a>
                                            <a class="twitter mx-2" href="#twitter">
                                                <span class="fa fa-twitter"></span>
                                            </a>
                                            <a class="google" href="#google-plus">
                                                <span class="fa fa-google-plus"></span>
                                            </a>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="team-bottom-block p-4">
                                <h5 class="member mb-1"><a href="#team">Teri Dac</a></h5>
                                <small>Manager of Company</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- //team setion --> --}}

        <!-- image with content section -->
        <section class="w3l-image-conten w3l-team">
            <div class="row no-gutters align-items-center">
                <div class="col-md-7 img-content-1">
                    <img src="{{ Storage::url($frase->files->filename) }}" class="img-fluid w-100" alt="" />
                </div>
                <div class="col-md-5 p-lg-5 p-md-3 p-4 my-md-0 my-3 text-center  img-content-2">
                    <h4 class="heading-styles"><i class="fa fa-quote-left mr-2" aria-hidden="true"></i> {!! strip_tags($frase->description) !!}</h4>
                </div>
            </div>
        </section>
    <!-- //image with content section -->

    @if ($testimonials->count() > 0)
        <x-Testimonials :items="$testimonials" />
    @endif
    
    @if($blogs->count() > 0)
        <x-Blogs :items="$blogs"/>
    @endif

    </x-slot>
</x-layout>