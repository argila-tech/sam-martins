<x-layout>
    @php
        $title = "Treinamentos";
    @endphp
    <x-slot name="css">
         <style>
            @media(min-width: 992px){
                .position-lg-absolute{
                    position: absolute;
                }
            }
        </style>
    </x-slot>
   
    <x-slot name="title">{{ $title }}</x-slot>
    <x-slot name="content">
        <!-- inner banner -->
        <div class="inner-banner">
            <div class="w3l-breadcrumb">
                <div class="container">
                    <h4 class="inner-text-title font-weight-bold text-white mb-sm-3 mb-2">{{ $title }}</h4>
                    <ul class="breadcrumbs-custom-path">
                        <li><a href="index.html">Home</a></li>
                        <li class="active"><span class="fa fa-chevron-right mx-2" aria-hidden="true"></span>{{ $title }}</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- //inner banner -->
        <!-- services section -->
        <section class="w3l-service-3">
            <div class="content-design-11 py-5">
                <div class="container py-md-5 py-4">
                    <div class="content-sec-11">
                        <div class="row">
                            @forelse ($items->lazy() as $item)
                                <div class="col-12">
                                    <div class="services-single d-flex p-sm-5 p-4">
                                        <div class="row">
                                            <div class="col-4">
                                                <img src="{{ $item->files->path }}" class="img-fluid w-100" alt="{{ $item->title }}">
                                            </div>
                                           <div class="col-8">
                                                <div class="services-content">
                                                    <h5><a href="{{ route('training',['slug' => $item->slug]) }}">{{ $item->title }}</a></h5>
                                                    {!! Str::limit(strip_tags($item->description),350,'...') !!}
                                                    <div class="d-lg-flex justify-content-between position-lg-absolute" style="width: 97%;bottom: -27px;">
                                                        <p class="read-button d-flex align-items-center mt-4 p-0" style="font-weight: 600; color: #707070; font-size: 15px; transition: all 0.3s ease-in;">
                                                            @if ($item->init_at != null)
                                                                Programação:
                                                                <span class="ml-2">
                                                                    @isset($item->init_at)
                                                                        {{ date('d/m', strtotime($item->init_at)) }}
                                                                    @endisset
                                                                    @if($item->finish_at != null)
                                                                         a {{ date('d/m/Y', strtotime($item->finish_at)) }}
                                                                    @endif
                                                                </span>
                                                            @endif
                                                        </p>
                                                        @isset($item->next)
                                                            <p class="read-button d-none d-xl-flex align-items-center mt-lg-4 p-0" style="font-weight: 600; color: #707070; font-size: 15px; transition: all 0.3s ease-in;">
                                                                Próxima Turma: 
                                                                <span class="ml-2">{{ $item->next }}</span>
                                                            </p>
                                                        @endisset                                                        
                                                        <a href="{{ route('training',['slug' => $item->slug]) }}" class="btn read-button d-flex align-items-center mt-2 mt-lg-4 p-0">
                                                            Saiba Mais<i class="fa fa-angle-double-right ml-1" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                           </div>                                            
                                        </div>                                        
                                    </div>
                                </div>
                            @empty
                                <div class="text-center p-5 w-100">
                                    <p>Ainda não há treinamento!</p>
                                </div>                                
                            @endforelse
                            <div class="col-12 paginate">
                                {{ $items->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- //services section -->
    </x-slot>
</x-layout>