<x-layout>
    <x-slot name="title">{{ $item->title }}</x-slot>
    <x-slot name="head">
        <meta name="keywords" content="{{ $settings['palavras-chaves'] ?? $item->tags ?? '' }}">

        <meta name="description" content="{!! strip_tags(str_replace('&nbsp;','',Str::limit($item->description, 500))) !!}">
        <meta name="subjet" content="{!! strip_tags(str_replace('&nbsp;','',Str::limit($item->description, 500))) !!}">
        <meta name="abstract" content="{!! strip_tags(str_replace('&nbsp;','',Str::limit($item->description, 500))) !!}">
        <meta name="topic" content="{!! strip_tags(str_replace('&nbsp;','',Str::limit($item->description, 500))) !!}">
        <meta name="summary" content="{!! strip_tags(str_replace('&nbsp;','',Str::limit($item->description, 500))) !!}">
        
        <meta property="og:description" content="{!! strip_tags(str_replace('&nbsp;','',Str::limit($item->description, 500))) !!}">
        <meta itemprop="description" content="{!! strip_tags(str_replace('&nbsp;','',Str::limit($item->description, 500))) !!}">
        <meta name="twitter:description" content="{!! strip_tags(str_replace('&nbsp;','',Str::limit($item->description, 500))) !!}">

        
        <meta property="og:url" content="{{ request()->fullUrl() }}">
        <meta property="og:type" content="website">
        <meta property="og:title" content="San Martin Inteligência Organizacional - {{ $item->title }}"> 
        <meta property="og:image" content="{{ asset($item->files->path) }}">
        
        <meta property="og:site_name" content="San Martin Inteligência Organizacional - {{ $item->title }}">
        <meta property="og:locale" content="pt_BR">
        <meta itemprop="name" content="San Martin Inteligência Organizacional - {{ $item->title }}">
        
        <meta itemprop="image" content="{{ asset($item->files->path) }}">
        <meta name="twitter:card" content="summary">
        <meta name="twitter:site" content="San Martin Inteligência Organizacional - {{ $item->title }}">
        <meta name="twitter:url" content="{{ request()->fullUrl() }}">
        <meta name="twitter:title" content="San Martin Inteligência Organizacional - {{ $item->title }}">
        <meta name="twitter:image" content="{{ asset($item->files->path) }}">
    </x-slot>
    <x-slot name="content">
        <!-- inner banner -->
        <div class="inner-banner">
            <div class="w3l-breadcrumb">
                <div class="container">
                    <h4 class="inner-text-title font-weight-bold text-white mb-sm-3 mb-2">{{ $item->title }}</h4>
                    <ul class="breadcrumbs-custom-path">
                        <li><a href="{{ route('index') }}">Home</a></li>
                        <li class="active"><span class="fa fa-chevron-right mx-2" aria-hidden="true"></span>{{ $item->title }}</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- //inner banner -->
        <section class="w3l-blog-single py-5">
            <div class="container py-md-5 py-4">
                <div class="row">
                    <div class="col-md-3 pr-lg-4 d-none d-md-block">
                        <div class="p-sticky-blog">
                            @foreach($items->lazy() as $it)
                                <aside class="mr-md-0 mr-3">
                                    <a href="{{ route('training',['slug' => $it->slug]) }}">
                                        <img src="{{ asset($it->files->path) }}" class="img-fluid" alt="{{ $it->title }}">
                                    </a>
                                    <h6 class="text-left-inner-9">
                                        <a href="{{ route('blog',['slug' => $it->slug]) }}">{{ $it->title }}</a>
                                    </h6>
                                    {{--  <span class="sub-inner-text-9"> Nov 06, 2020</span>  --}}
                                    <hr>
                                </aside>
                            @endforeach
                        </div>
                    </div>                    
                    <div class="col-md-9 pl-lg-5 mb-md-0 mb-5">
                        <p class="text-tag">{{ $item->title }}</p>
                        <h4 class="text-head-text-9"><a href="#text" class="m-0">{{ $item->subtitle }}</a></h4>
                        @isset($item->files)
                            <div class="py-3 pb-5">
                                <figure class="figure">
                                    <img src="{{ asset($item->files->path) }}" class="figure-img img-fluid w-100" alt="{{ $item->title }}">                                
                                </figure>
                            </div>
                        @endisset    
                        {!! $item->description !!}
                        <div class="row py-5">
                            @isset($item->price)
                                <h3 class="text-tag w-100">Investimento</h3>
                                <p class="pt-3 w-100">À vista: <strong>{{ 'R$ '.number_format($item->price, 2, ',', '.') }}</strong> ou parcelado no cartão de crédito em: <strong>{{ $item->installments }}x</strong></p>
                            @endisset
                        </div>
                        @if($item->modules->count() > 0)
                            <div class="row">
                                <h3 class="text-tag w-100">Módulos</h3>
                                @foreach ($item->modules()->orderBy('start_at','asc')->get()->lazy() as $module)
                                {{-- {{ dd($module->teams) }} --}}
                                    <div class="col-md-6 mt-md-0 mt-5 py-4">
                                        <div class="customer-single services-single pr-lg-4 p-4">
                                            <h6><strong>{{ $module->title }}</strong></h6>
                                            <blockquote class="pt-2">{!! $module->description !!}</blockquote>
                                            <div class="customer-img d-flex align-items-center mt-4">
                                                <img src="{{ asset($module->teams->files->path) }}" class="img-fluid mr-3" alt="{{ $item->title }}"  style="width: 40px; height: 40px;"/>
                                                <div class="customer-info">
                                                    <h6>{{ $module->teams->title }}</h6>
                                                    <small class="mb-0">{{ $module->teams->office }}</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                        <div class="row pt-5">
                            @isset($item->address)
                                <h3 class="text-tag w-100">Localização</h3>
                                <p>{{ $item->address }}</p>
                                @isset($item->map)
                                    <div class="py-4 w-100">
                                        {!! $item->map !!}
                                    </div>
                                @endisset                                
                            @endisset
                        </div>
                    </div>            
                    <div class="col-12 py-5">
                        <h3 class="text-tag w-100 pb-5">Faça sua pré Inscrição e garanta sua vaga.</h3>
                        @isset($settings['whatsapp'])
                            <a href="https://api.whatsapp.com/send?phone=+55{{ $whatslink }}&text=Olá,%20quero%20mais%20informações%20sobre%20o%20treinamento:%20{{ route('training',['slug' => $item->slug]) }}" target="_blank" rel="noopener noreferrer" class="btn btn-style-white btn-style-primary px-2">
                                <img src="{{ asset('assets/img/whatsapp.png') }}" style="width: 30px" alt="Whatsapp"> Clique aqui
                            </a>
                        @endisset
                        {{-- <div class="contact-form">
                            <form action="{{ route('sendTrainings') }}" method="post" class="cont-form">
                                @csrf
                                <input type="hidden" name="trainings_id" value="{{ $item->id }}">
                                <div class="row">
                                    <div class="col-12 col-md-4">
                                        <x-input type="text" name="name" placeholder="Nome e Sobrenome"  class="contact-input" />
                                        <x-error field="name" class="text-danger" />
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <x-input type="text" name="phone" placeholder="Whatsapp" class="contact-input sp_celphones" />
                                        <x-error field="phone" class="text-danger" />
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <x-input type="email" name="email" placeholder="E-mail" class="contact-input" />
                                        <x-error field="email" class="text-danger" />
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button class="btn btn-style-white btn-style-primary">Enviar</button>
                                </div>
                            </form>
                        </div> --}}
                    </div>
                </div>
        </section>
    </x-slot>
</x-layout>
<x-scripts>
    <x-slot name="js">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script>
            var SPMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };
            $('.sp_celphones').mask(SPMaskBehavior, spOptions);
        </script>
    </x-slot>
</x-scripts>