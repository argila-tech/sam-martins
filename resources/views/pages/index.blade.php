<x-layout>
    <x-slot name="content">
        <x-Slides :items="$slides" />
        <x-About :items="$about"/>
        <x-Services :items="$services"/>
         {{-- <x-Insigners />  --}}
        <x-Testimonials :items="$testimonials"/>
        @if($blogs->count() > 0)
            <x-Blogs :items="$blogs"/>
        @endif
    </x-slot>
</x-layout>