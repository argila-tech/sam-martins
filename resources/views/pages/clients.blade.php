<x-layout>
    @php
        $title = "Alguns de nossos Clientes";
    @endphp
    <x-slot name="title">{{ $title }}</x-slot>
    <x-slot name="css">
        <style>
            .content-sec-11 img{
                filter: grayscale(5);
            }
            .content-sec-11 img:hover{
                filter: unset;
            }
        </style>
    </x-slot>
    <x-slot name="content">
        <!-- inner banner -->
        <div class="inner-banner">
            <div class="w3l-breadcrumb">
                <div class="container">
                    <h4 class="inner-text-title font-weight-bold text-white mb-sm-3 mb-2">{{ $title }}</h4>
                    <ul class="breadcrumbs-custom-path">
                        <li><a href="index.html">Home</a></li>
                        <li class="active"><span class="fa fa-chevron-right mx-2" aria-hidden="true"></span>{{ $title }}</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- //inner banner -->
        <!-- services section -->
        <section class="w3l-service-3">
            <div class="content-design-11 py-5">
                <div class="container py-md-5 py-4">
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <div class="section-heading text-center mb-sm-5 mb-4">
                                {{-- <h3 class="title-style mb-2">Alguns de nossos Clientes</h3> --}}
                                <p class="lead">
                                    Conheça as empresas que são nossas maiores parceiras. A San Martin tem orgulho de contribuir para o desenvolvimento humano e organizacional dessas marcas.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="content-sec-11">
                        <div class="row">
                            @foreach ($items->lazy() as $item)
                                <div class="col-6 col-md-3 col-lg-4 col-xl-3 py-3">
                                    <img src="{{ $item->files->path }}" class="img-fluid" alt="{{ $item->title }}">
                                </div>
                            @endforeach
                        </div>
                        <div class="col-12 paginate pt-5">
                            {{ $items->links() }}
                        </div>
                </div>
                </div>
            </div>
        </section>
        <!-- //services section -->
    </x-slot>
</x-layout>