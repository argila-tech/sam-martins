<x-layout>
    @php
        $title = "Serviços";
    @endphp
    <x-slot name="title">{{ $title }}</x-slot>
    <x-slot name="content">
        <!-- inner banner -->
        <div class="inner-banner">
            <div class="w3l-breadcrumb">
                <div class="container">
                    <h4 class="inner-text-title font-weight-bold text-white mb-sm-3 mb-2">{{ $title }}</h4>
                    <ul class="breadcrumbs-custom-path">
                        <li><a href="index.html">Home</a></li>
                        <li class="active"><span class="fa fa-chevron-right mx-2" aria-hidden="true"></span>{{ $title }}</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- //inner banner -->
        <!-- services section -->
        <section class="w3l-service-3">
            <div class="content-design-11 py-5">
                <div class="container py-md-5 py-4">
                    <div class="content-sec-11">
                        <div class="row">
                            @foreach ($items->lazy() as $item)
                                <div class="col-12 py-2">
                                    <div class="services-single d-flex p-sm-5 p-4">
                                        <div class="service-icon mr-sm-4 mr-3">
                                            {{-- <span class="fa fa-headphones" aria-hidden="true"></span> --}}
                                            {!! $item->icon !!}
                                        </div>
                                        <div class="services-content">
                                            <h5 class="pb-2"><a href="{{ route('service',['slug' => $item->slug]) }}">{{ $item->title }}</a></h5>
                                            {!! $item->description !!}
                                            {{-- <a href="#services" class="btn read-button d-flex align-items-center mt-4 p-0">Read --}}
                                                {{-- More<i class="fa fa-angle-double-right ml-1" aria-hidden="true"></i></a> --}}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="col-12 paginate">
                                {{ $items->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- //services section -->
    </x-slot>
</x-layout>