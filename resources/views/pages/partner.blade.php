<x-layout>
    <x-slot name="title">{{ $item->title }}</x-slot>
    <x-slot name="head">
        <meta name="keywords" content="{{ $settings['palavras-chaves'] }}">

        <meta name="description" content="{!! strip_tags(str_replace('&nbsp;','',Str::limit($item->description, 500))) !!}">
        <meta name="subjet" content="{!! strip_tags(str_replace('&nbsp;','',Str::limit($item->description, 500))) !!}">
        <meta name="abstract" content="{!! strip_tags(str_replace('&nbsp;','',Str::limit($item->description, 500))) !!}">
        <meta name="topic" content="{!! strip_tags(str_replace('&nbsp;','',Str::limit($item->description, 500))) !!}">
        <meta name="summary" content="{!! strip_tags(str_replace('&nbsp;','',Str::limit($item->description, 500))) !!}">
        
        <meta property="og:description" content="{!! strip_tags(str_replace('&nbsp;','',Str::limit($item->description, 500))) !!}">
        <meta itemprop="description" content="{!! strip_tags(str_replace('&nbsp;','',Str::limit($item->description, 500))) !!}">
        <meta name="twitter:description" content="{!! strip_tags(str_replace('&nbsp;','',Str::limit($item->description, 500))) !!}">

        
        <meta property="og:url" content="{{ request()->fullUrl() }}">
        <meta property="og:type" content="website">
        <meta property="og:title" content="San Martin Inteligência Organizacional - {{ $item->title }}"> 
        <meta property="og:image" content="{{ asset($item->files->path) }}">
        
        <meta property="og:site_name" content="San Martin Inteligência Organizacional - {{ $item->title }}">
        <meta property="og:locale" content="pt_BR">
        <meta itemprop="name" content="San Martin Inteligência Organizacional - {{ $item->title }}">
        
        <meta itemprop="image" content="{{ asset($item->files->path) }}">
        <meta name="twitter:card" content="summary">
        <meta name="twitter:site" content="San Martin Inteligência Organizacional - {{ $item->title }}">
        <meta name="twitter:url" content="{{ request()->fullUrl() }}">
        <meta name="twitter:title" content="San Martin Inteligência Organizacional - {{ $item->title }}">
        <meta name="twitter:image" content="{{ asset($item->files->path) }}">
    </x-slot>
    
    <x-slot name="content">
        <!-- inner banner -->
        <div class="inner-banner">
            <div class="w3l-breadcrumb">
                <div class="container">
                    <h4 class="inner-text-title font-weight-bold text-white mb-sm-3 mb-2">{{ $item->title }}</h4>
                    <ul class="breadcrumbs-custom-path">
                        <li><a href="{{ route('index') }}">Home</a></li>
                        <li class="active"><span class="fa fa-chevron-right mx-2" aria-hidden="true"></span>{{ $item->title }}</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- //inner banner -->
        <section class="w3l-blog-single py-5">
            <div class="container py-md-5 py-4">
                <div class="row">
                    <div class="col-md-3 left-blog-single pr-lg-4">
                        <div class="p-sticky-blog">
                            
                            @foreach($items->lazy() as $it)
                                <aside class="mr-md-0 mr-3">
                                    <a href="{{ route('partner',['slug' => $it->slug]) }}">
                                        <img src="{{ asset($it->files->path) }}" class="img-fluid" alt="{{ $it->title }}">
                                    </a>
                                    <h6 class="text-left-inner-9">
                                        <a href="{{ route('partner',['slug' => $it->slug]) }}">{{ $it->title }}</a>
                                    </h6>
                                    {{-- <span class="sub-inner-text-9"> Nov 06, 2020</span> --}}
                                    <hr>
                                </aside>
                            @endforeach
                        </div>
                    </div>
                    
                    <div class="col-md-9 right-blog-single pl-lg-5 mb-md-0 mb-5">
                        <h4 class="text-head-text-9"><a href="#text" class="m-0">{{ $item->title }}</a></h4>
                        @isset($item->files)
                        <div class="py-3 pb-5">
                            <figure class="figure">
                                <img src="{{ asset($item->files->path) }}" class="figure-img img-fluid" alt="{{ $item->title }}">
                                
                            </figure>
                        </div>
                        @endisset
                        
                        {!! $item->description !!}


                        <div class="row left-right pt-5">
                            <div class="col-sm-6 buttons-singles tags"></div>
                            <div class="col-sm-6 buttons-singles mt-sm-0 mt-2">
                                <h4>Saiba Mais :</h4>
                                @isset($item->site)
                                    <a href="{{ $item->site }}" target="_blank" rel="noopener noreferrer"><span class="fab fa-internet-explorer" aria-hidden="true"></span></a>
                                @endisset
                                @isset($item->facebook)
                                    <a href="{{ $item->facebook }}" target="_blank" rel="noopener noreferrer"><span class="fab fa-facebook" aria-hidden="true"></span></a>
                                @endisset
                                @isset($item->instagram)
                                     <a href="{{ $item->facebook }}" target="_blank" rel="noopener noreferrer"><span class="fab fa-instagram" aria-hidden="true"></span></a>
                                @endisset
                                @isset($item->whatsapp)
                                    @php
                                        $numberWhats = preg_replace('/(\D+)/', '', $item->whatsapp);
                                        $numberWhats = preg_replace('/ˆ0?(\d{2})9?(\d{8})/', '$1$2', $numberWhats);
                                    @endphp 
                                     <a href="https://api.whatsapp.com/send?phone=+55{{ $numberWhats }}&text=oi" target="_blank" rel="noopener noreferrer"><span class="fab fa-whatsapp" aria-hidden="true"></span></a>
                                @endisset
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </x-slot>
</x-layout>