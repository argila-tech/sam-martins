<x-layout>
    <x-slot name="title">{{ $item->title }}</x-slot>

    <x-slot name="head">
        @isset($settings['palavras-chaves'])
            <meta name="keywords" content="{{ $settings['palavras-chaves'] }}">
        @endisset

        <meta name="description" content="{!! strip_tags(str_replace('&nbsp;','',Str::limit($item->description, 500))) !!}">
        <meta name="subjet" content="{!! strip_tags(str_replace('&nbsp;','',Str::limit($item->description, 500))) !!}">
        <meta name="abstract" content="{!! strip_tags(str_replace('&nbsp;','',Str::limit($item->description, 500))) !!}">
        <meta name="topic" content="{!! strip_tags(str_replace('&nbsp;','',Str::limit($item->description, 500))) !!}">
        <meta name="summary" content="{!! strip_tags(str_replace('&nbsp;','',Str::limit($item->description, 500))) !!}">
        
        <meta property="og:description" content="{!! strip_tags(str_replace('&nbsp;','',Str::limit($item->description, 500))) !!}">
        <meta itemprop="description" content="{!! strip_tags(str_replace('&nbsp;','',Str::limit($item->description, 500))) !!}">
        <meta name="twitter:description" content="{!! strip_tags(str_replace('&nbsp;','',Str::limit($item->description, 500))) !!}">

        
        <meta property="og:url" content="{{ request()->fullUrl() }}">
        <meta property="og:type" content="website">
        <meta property="og:title" content="San Martin Inteligência Organizacional - {{ $item->title }}"> 
        <meta property="og:image" content="{{ asset('assets/img/icon.png') }}">
        
        <meta property="og:site_name" content="San Martin Inteligência Organizacional - {{ $item->title }}">
        <meta property="og:locale" content="pt_BR">
        <meta itemprop="name" content="San Martin Inteligência Organizacional - {{ $item->title }}">
        
        <meta itemprop="image" content="{{ asset('assets/img/icon.png') }}">
        <meta name="twitter:card" content="summary">
        <meta name="twitter:site" content="San Martin Inteligência Organizacional - {{ $item->title }}">
        <meta name="twitter:url" content="{{ request()->fullUrl() }}">
        <meta name="twitter:title" content="San Martin Inteligência Organizacional - {{ $item->title }}">
        <meta name="twitter:image" content="{{ asset('assets/img/icon.png') }}">
    </x-slot>

    <x-slot name="content">
        <!-- inner banner -->
        <div class="inner-banner">
            <div class="w3l-breadcrumb">
                <div class="container">
                    <h4 class="inner-text-title font-weight-bold text-white mb-sm-3 mb-2">{{ $item->title }}</h4>
                    <ul class="breadcrumbs-custom-path">
                        <li><a href="{{ route('index') }}">Home</a></li>
                        <li class="active"><span class="fa fa-chevron-right mx-2" aria-hidden="true"></span>{{ $item->title }}</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- //inner banner -->
        <section class="w3l-blog-single py-5">
            <div class="container py-md-5 py-4">
                <div class="row">
                    <div class="col-md-3 left-blog-single pr-lg-4">
                        <div class="p-sticky-blog">
                            @foreach($items->lazy() as $it)
                                <aside class="mr-md-0 mr-3">                                        
                                    <h6 class="text-left-inner-9">
                                        <a href="{{ route('service',['slug' => $it->slug]) }}">{{ $it->title }}</a>
                                    </h6>
                                    <span class="sub-inner-text-9">{!! strip_tags(Str::limit($it->description,100)) !!}</span>
                                    <hr>
                                </aside>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-9 right-blog-single pl-lg-5 mb-md-0 mb-5">
                        <h4 class="text-head-text-9 pb-5"><a href="#text" class="m-0">{{ $item->title }}</a></h4>
                        {!! $item->description !!}
                        <div class="mt-md-5 mt-4 text-center">
                            {{-- <a class="btn btn-style-white btn-style-primary" href="{{ route('contacts',['slug' => $item->slug]) }}">Solicite</a> --}}
                            @isset($settings['whatsapp'])
                                <a href="https://api.whatsapp.com/send?phone=+55{{ $whatslink }}&text=Olá,%20quero%20mais%20informações%20sobre%20o%servico:%20{{ route('service',['slug' => $item->slug]) }}" target="_blank" rel="noopener noreferrer" class="btn btn-style-white btn-style-primary px-2">
                                    <img src="{{ asset('assets/img/whatsapp.png') }}" style="width: 30px" alt="Whatsapp"> Clique aqui
                                </a>
                        @endisset
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </x-slot>
</x-layout>