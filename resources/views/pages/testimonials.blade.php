<x-layout>
    <x-slot name="title">{{ 'Depoimentos' }}</x-slot>
    <x-slot name="css">
        <style>
            .customer-single{
                height: 400px;
            }
        </style>
    </x-slot>
    <x-slot name="content">
        <!-- inner banner -->
        <div class="inner-banner">
            <div class="w3l-breadcrumb">
                <div class="container">
                    <h4 class="inner-text-title font-weight-bold text-white mb-sm-3 mb-2">{{ 'Depoimentos' }}</h4>
                    <ul class="breadcrumbs-custom-path">
                        <li><a href="{{ route('index') }}">Home</a></li>
                        <li class="active"><span class="fa fa-chevron-right mx-2" aria-hidden="true"></span>{{ 'Depoimentos' }}</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- //inner banner -->
    <!-- customers section -->
    <section class="w3l-customer-block py-5">
        <div class="container py-md-5 py-4">
            <div class="row">
                {{-- <div class="col-12">
                    <div class="section-heading mb-sm-5 mb-4">
                        <h3 class="title-style mb-2">Depoimentos</h3>
                    </div>
                </div> --}}
                @foreach ($items->lazy() as $item)
                    <div class="col-md-6 col-lg-4 mt-md-0 mt-5 py-4">
                        <div class="customer-single services-single pr-lg-4 p-4">
                            <blockquote>{!! $item->description !!}</blockquote>
                            <div class="customer-img d-flex align-items-center mt-4">
                                <div style="width: 70px; display: flex; justify-content: center; margin-right: 15px;">
                                    <div style="background-image: url('{{ asset(@$item->files->path) }}'); width: 60px; height: 60px; background-size: cover; background-position: center; border-radius: 100%;">
                                        <img src="{{ asset(@$item->files->path) }}" class="d-none" alt="{{ $item->office }}" />
                                    </div>
                                </div>                                
                                <div class="customer-info">
                                    <h6>{{ $item->title }}</h6>
                                    <small class="mb-0">{{ $item->office }}</small>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="col-12 paginate pt-5">
                {{ $items->links() }}
            </div>
        </div>
    </section>
    <!-- //customers section -->
            
    </x-slot>
</x-layout>