<x-layout>
    @php
        $title = "Fale Conosco";
    @endphp
    {{--  {{ dd($settings) }}  --}}
    <x-slot name="title">{{ $title }}</x-slot>
    <x-slot name="content">
        <!-- inner banner -->
        <div class="inner-banner">
            <section class="w3l-breadcrumb">
                <div class="container">
                    <h4 class="inner-text-title font-weight-bold text-white mb-sm-3 mb-2">{{ $title }}</h4>
                    <ul class="breadcrumbs-custom-path">
                        <li><a href="{{ route('index') }}">Home</a></li>
                        <li class="active"><span class="fa fa-chevron-right mx-2" aria-hidden="true"></span>{{ $title }}</li>
                    </ul>
                </div>
            </section>
        </div>
        <!-- //inner banner -->

        <!-- contact page -->
        <div class="contact-form py-5">
            <div class="container py-lg-5 py-4">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="section-heading text-center mb-sm-5 mb-4">
                            <h3 class="title-style mb-2">Fale Conosco</h3>
                            <p class="lead">
                                Quer saber mais sobre nossos serviços? Preencha o formulário abaixo ou entre em contato direto conosco para obter informações detalhadas.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="mx-auto" style="max-width:1100px">
                    <div class="row">
                        @isset($settings['linkedin'])
                            <div class="col-lg-4 col-md-6">
                                <div class="contact-address p-4">
                                    <div class="contact-icon d-flex align-items-center">
                                        <img src="{{ asset('assets/img/icons/linkedin.png') }}" width="18px" alt="Linkdin">
                                        <div class="ml-3">
                                            <h5 class="contact-text">Linkedin:</h5>
                                            <p>
                                                <a href="{{ $settings['linkedin'] }}">
                                                    Linkedin
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endisset
                        @isset($settings['contato'])
                            <div class="col-lg-4 col-md-6 mt-md-0 mt-4">
                                <div class="contact-address p-4">
                                    <div class="contact-icon d-flex align-items-center">
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                        <div class="ml-3">
                                            <h5 class="contact-text">Telefone:</h5>
                                            <a href="tel:+55{{ $contato }}">{{ $settings['contato'] }}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endisset
                        @isset($settings['email-contato'])                        
                            <div class="col-lg-4 col-md-6 mt-lg-0 mt-4">
                                <div class="contact-address p-4">
                                    <div class="contact-icon d-flex align-items-center">
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                        <div class="ml-3">
                                            <h5 class="contact-text">Email:</h5>
                                            <a href="mailto:{{ $settings['email-contato'] }}"> {{ $settings['email-contato'] }}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endisset
                    </div>
                    <div class="contact-form pt-5 mt-2">
                        <form action="{{ route('sendContacts') }}" method="post" class="cont-form p-sm-5">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" name="name" placeholder="Nome e Sobrenome"
                                        class="contact-input" value="{{ old('name') }}" />
                                        <x-error field="name" class="text-danger" />
                                    <input type="text" name="phone" placeholder="Whatsapp"
                                        class="contact-input sp_celphones" value="{{ old('phone') }}" />
                                        <x-error field="phone" class="text-danger" />
                                </div>
                                <div class="col-md-6">
                                    <input type="email" name="email" placeholder="E-mail"
                                        class="contact-input" value="{{ old('email') }}" />
                                        <x-error field="email" class="text-danger" />
                                    <input type="text" name="subject" placeholder="Assunto"
                                        class="contact-input" value="{{ $assunto }}" {{ $assunto != null ? 'disabled' : '' }} />
                                        <x-error field="subject" class="text-danger" />
                                </div>
                            </div>
                            <textarea class="contact-textarea" name="message"
                                placeholder="Mensagem">{{ old('message') }}</textarea>
                                <x-error field="message" class="text-danger" />
                            <div class="text-right">
                                <button class="btn btn-style-white btn-style-primary">Enviar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="map">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d387190.2895687731!2d-74.26055986835598!3d40.697668402590374!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sin!4v1562582305883!5m2!1sen!2sin"
                frameborder="0" style="border:0" allowfullscreen=""></iframe>
        </div> --}}
        <!-- //contact page -->
    </x-slot>
</x-layout>

<x-scripts>
    <x-slot name="js">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script>
            var SPMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };
            $('.sp_celphones').mask(SPMaskBehavior, spOptions);
        </script>
    </x-slot>
</x-scripts>