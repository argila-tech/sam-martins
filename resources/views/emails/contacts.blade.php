@component('mail::message')
# Introduction

Novo contato através do site

@component('mail::button', ['url' => route('dashboard.contacts.show', $item)])
Clique aqui
@endcomponent

Obrigado,<br>
{{ config('app.name') }}
@endcomponent
