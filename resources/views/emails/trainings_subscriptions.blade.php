@component('mail::message')
# Introduction

Novo pré inscrito

@component('mail::button', ['url' => route('dashboard.subscriptions.show', $sub)])
Clique aqui
@endcomponent

Obigado,<br>
{{ config('app.name') }}
@endcomponent
