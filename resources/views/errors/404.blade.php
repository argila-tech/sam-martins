<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>{{ env('APP_NAME') }}</title>
        <link href="//fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        {{ @$css }}
    </head>
    <body>
        <div class="w3l-error-page">
            <div class="container">
                <div class="w3l-error-block text-center">
                    <div class="content mx-auto position-relative" style="max-width:600px">
                        <h2>Oops!</h2>
                        <h1>404</h1>
                        <p class="text-white">A página que você está procurando pode ter sido removida caso seu nome tenha sido alterado ou temporariamente indisponível.</p>
                        {{-- <form action="#" method="GET" class="search-error position-relative mt-4 mx-auto" style="max-width:450px">
                            <input type="search" class="search-input" placeholder="Search here.." required="">
                            <button class="btn error-btn"><span class="fa fa-search"></span></button>
                        </form> --}}
                        <a href="{{ route('index') }}" class="btn btn-style-white mt-5">Ir para Home</a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>