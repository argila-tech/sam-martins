<!-- banner section -->
{{-- <section class="">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            @foreach ($items->lazy() as $item)
                <div class="carousel-item {{ $loop->first ? 'active' : ''}} {{ $item->type == "desktop" ? "d-none d-md-block" : "d-block d-md-none"}}">
                    <a href="{{ $item->link ?? '#' }}" target="_blank" rel="noopener noreferrer">
                        <img src="{{ $item->files->path }}" class="d-block w-100" alt="{{ $item->title }}">
                    </a>
                </div>
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section> --}}

{{-- <x-slot name="css">
    <style>
        .slide-carousel .owl-prev span,
        .slide-carousel .owl-next span{
        font-size: 5rem;
        }
    </style>
</x-slot> --}}

<div class="owl-carousel owl-theme d-none d-md-flex slide-carousel">
    @foreach ($items->lazy() as $item)
        @if ($item->type == "desktop")
            <div class="item">
                <a href="{{ $item->link ?? '#' }}" target="_blank" rel="noopener noreferrer">
                    <img src="{{ $item->files->path }}" class="d-block w-100" alt="{{ $item->title }}">
                </a>
            </div> 
        @endif           
    @endforeach
</div>
<div class="owl-carousel owl-theme d-md-none slide-carousel">
    @foreach ($items->lazy() as $item)
        @if ($item->type == "mobile")
            <div class="item">
                <a href="{{ $item->link ?? '#' }}" target="_blank" rel="noopener noreferrer">
                    <img src="{{ $item->files->path }}" class="d-block w-100" alt="{{ $item->title }}">
                </a>
            </div> 
        @endif           
    @endforeach
</div>
<!-- //banner section -->