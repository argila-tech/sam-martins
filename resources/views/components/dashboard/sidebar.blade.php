@php
    $items = [
        [
            'name' => 'Dashboard',
            'icon' => "<i class='ni ni-tv-2 text-primary'></i>",
            'link' => 'home'
        ],
        [
            'name' => 'Slides',
            'icon' => "<i class='fab fa-slideshare' style='color: #1c1ec4'></i>",
            'link' => 'dashboard.slides.index',
            'links' => [
                ['link' => 'dashboard.slides.index'],
                ['link' => 'dashboard.slides.create'],
                ['link' => 'dashboard.slides.show'],
                ['link' => 'dashboard.slides.edit']
            ]
        ],
        [
            'name' => 'Blog',
            'icon' => "<i class='fa fa-rss' style='color: #f89e36'></i>",
            'link' => 'dashboard.blog.index',
            'links' => [
                ['link' => 'dashboard.blog.index'],
                ['link' => 'dashboard.blog.create'],
                ['link' => 'dashboard.blog.show'],
                ['link' => 'dashboard.blog.edit']
            ]
        ],
        [
            'name' => 'Serviços',
            'icon' => "<i class='ni ni-briefcase-24' style='color: #c41c5c'></i>",
            'link' => 'dashboard.services.index',
            'links' => [
                ['link' => 'dashboard.services.index'],
                ['link' => 'dashboard.services.create'],
                ['link' => 'dashboard.services.show'],
                ['link' => 'dashboard.services.edit']
            ]
        ],
        [
            'name' => 'Páginas Estáticas',
            'icon' => "<i class='fas fa-columns' style='color: #037562'></i>",
            'link' => 'dashboard.pages.index',
            'links' => [
                ['link' => 'dashboard.pages.index'],
                ['link' => 'dashboard.pages.create'],
                ['link' => 'dashboard.pages.show'],
                ['link' => 'dashboard.pages.edit']
            ]
        ],
        [
            'name' => 'Depoimentos',
            'icon' => "<i class='fas fa-quote-right' style='color: #5f3c2b'></i>",
            'link' => 'dashboard.testimonials.index',
            'links' => [
                ['link' => 'dashboard.testimonials.index'],
                ['link' => 'dashboard.testimonials.create'],
                ['link' => 'dashboard.testimonials.show'],
                ['link' => 'dashboard.testimonials.edit']
            ]
        ],
        [
            'name' => 'Clients',
            'icon' => "<i class='fas fa-gem' style='color: #e64949'></i>",
            'link' => 'dashboard.clients.index',
            'links' => [
                ['link' => 'dashboard.clients.index'],
                ['link' => 'dashboard.clients.create'],
                ['link' => 'dashboard.clients.show'],
                ['link' => 'dashboard.clients.edit']
            ]
        ],
        [
            'name' => 'Parceiros',
            'icon' => "<i class='fas fa-gem' style='color: #e64949'></i>",
            'link' => 'dashboard.partners.index',
            'links' => [
                ['link' => 'dashboard.partners.index'],
                ['link' => 'dashboard.partners.create'],
                ['link' => 'dashboard.partners.show'],
                ['link' => 'dashboard.partners.edit']
            ]
        ],
    ];

    $trainings = [
        [
            'name' => 'Categorias',
            'icon' => "<i class='fas fa-shopping-cart' style='color: #66965c'></i>",
            'link' => 'dashboard.categorys.index',
            'links' => [
                ['link' => 'dashboard.categorys.index'],
                ['link' => 'dashboard.categorys.create'],
                ['link' => 'dashboard.categorys.show'],
                ['link' => 'dashboard.categorys.edit']
            ]
        ],
        [
            'name' => 'Treinamentos',
            'icon' => "<i class='fas fa-brain' style='color: #00a8db'></i>",
            'link' => 'dashboard.trainings.index',
            'links' => [
                ['link' => 'dashboard.trainings.index'],
                ['link' => 'dashboard.trainings.create'],
                ['link' => 'dashboard.trainings.show'],
                ['link' => 'dashboard.trainings.edit']
            ]
        ],
        [
            'name' => 'Subinscritos',
            'icon' => "<i class='fas fa-shopping-cart' style='color: #9ea036'></i>",
            'link' => 'dashboard.subscriptions.index',
            'links' => [
                ['link' => 'dashboard.subscriptions.index'],
                ['link' => 'dashboard.subscriptions.create'],
                ['link' => 'dashboard.subscriptions.show'],
                ['link' => 'dashboard.subscriptions.edit']
            ]
        ],
        [
            'name' => 'Equipe',
            'icon' => "<i class='fas fa-users' style='color: #33b4ff'></i>",
            'link' => 'dashboard.teams.index',
            'links' => [
                ['link' => 'dashboard.teams.index'],
                ['link' => 'dashboard.teams.create'],
                ['link' => 'dashboard.teams.show'],
                ['link' => 'dashboard.teams.edit']
            ]
        ],
    ];

    $settings = [
        [
            'name' => 'Minha Conta',
            'icon' => "<i class='fas fa-user-circle' style='color: #9b1eee'></i>",
            'link' => 'dashboard.profile.edit',
        ],
        [
            'name' => 'Configurações',
            'icon' => "<i class='fas fa-cogs' style='color: #d83bd8'></i>",
            'link' => 'dashboard.settings.index',
            'links' => [
                ['link' => 'dashboard.settings.index'],
                ['link' => 'dashboard.settings.create'],
                ['link' => 'dashboard.settings.show'],
                ['link' => 'dashboard.settings.edit']
            ]
        ]
    ];
@endphp
<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main"
            aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand pt-0" href="{{ route('home') }}">
            <img src="{{ asset('assets/img/logo-admin.png') }}" class="navbar-brand-img w-100" alt="{{ env('APP_NAME') }}" style="width: 242px;">
        </a>
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    <div class="media align-items-center">
                        <span class="avatar-sm rounded-circle">
                            <img alt="Image placeholder" src="{{ asset('assets/img/brand/favicon.png') }}">
                            {{-- Admin --}}
                        </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">{{ __('Welcome!') }}</h6>
                    </div>
                    <a href="{{ route('dashboard.profile.edit') }}" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span>{{ __('My profile') }}</span>
                    </a>
                    <a href="#" class="dropdown-item">
                        <i class="ni ni-settings-gear-65"></i>
                        <span>{{ __('Settings') }}</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        <i class="ni ni-user-run"></i>
                        <span>{{ __('Logout') }}</span>
                    </a>
                </div>
            </li>
        </ul>
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset('assets/img/logo-admin.png') }}" class="navbar-brand-img w-100" alt="{{ env('APP_NAME') }}" style="width: 242px;">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse"
                            data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false"
                            aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <h6 class="navbar-heading text-muted">Site</h6>
            <ul class="navbar-nav">
                @foreach ($items as $item)
                    <li class="nav-item">
                        <a class="nav-link @isset($item['links']) @foreach ($item['links'] as
                            $link) {{ Route::is($link['link']) ? 'active' : '' }} @endforeach @endisset" href="{{ route($item['link']) }}">
                            {!! $item['icon'] !!} {{ __($item['name']) }}
                        </a>
                    </li>
                @endforeach
            </ul>
            <hr class="my-3">
            <h6 class="navbar-heading text-muted">Treinamentos</h6>
            <ul class="navbar-nav">
                 @foreach ($trainings as $item)
                    <li class="nav-item">
                        <a class="nav-link @isset($item['links']) @foreach ($item['links'] as
                            $link) {{ Route::is($link['link']) ? 'active' : '' }} @endforeach @endisset" href="{{ route($item['link']) }}">
                            {!! $item['icon'] !!} {{ __($item['name']) }}
                        </a>
                    </li>
                @endforeach
            </ul>


            <hr class="my-3">
            <h6 class="navbar-heading text-muted">Contatos</h6>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('dashboard.contacts') ? 'active' : '' }}" href="{{ route('dashboard.contacts.index') }}">
                        <i class="fas fa-bullhorn text-danger"></i>Contatos
                    </a>
                </li>
            </ul>
            <hr class="my-3">
            <h6 class="navbar-heading text-muted">Configurações</h6>
            <ul class="navbar-nav">
                @foreach ($settings as $item)
                    <li class="nav-item">
                        <a class="nav-link @isset($item['links']) @foreach ($item['links'] as
                            $link) {{ Route::is($link['link']) ? 'active' : '' }} @endforeach @endisset" href="{{ route($item['link']) }}">
                            {!! $item['icon'] !!} {{ __($item['name']) }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</nav>
