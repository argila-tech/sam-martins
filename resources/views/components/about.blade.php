<!-- about -->
<section class="w3l-features-photo-7 py-5">
    <div class="container py-md-5 py-4">
        <div class="row w3l-features-photo-7_top">
            <div class="col-12">
                {{-- <h4 class="title-style mb-2">{{ $items->title }}</h4> --}}
            </div>
            <div class="col-lg-6 w3l-features-photo-7_top-left pr-lg-5">
                
                <p>{!! $items->description !!}</p>
                {{-- <div class="row feat_top mt-4 pt-lg-3">
                    <div class="col-6 w3l-features-photo-7-box">
                        <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                        <h5 class="w3l-feature-text my-2">Our Mission</h5>
                        <p>In a diam et dui elit, orci urna vel id neque. Donec sed tempus enims.</p>
                    </div>
                    <div class="col-6 w3l-features-photo-7-box">
                        <i class="fa fa-angellist" aria-hidden="true"></i>
                        <h5 class="w3l-feature-text my-2">Our Vision</h5>
                        <p>Et dui elit, orci urnavel id neque. Donec in a diam sed tempus trsma.</p>
                    </div>
                </div> --}}
            </div>
            <div class="col-lg-6 w3l-features-photo-7_top-right mt-lg-0 mt-sm-5 mt-4">
                <img src="{{ $items->files->path }}" class="img-responsive" alt="" />
            </div>
        </div>
    </div>
</section>
<!-- //about -->