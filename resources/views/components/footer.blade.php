<!-- call section -->
@isset($contato)
    <section class="w3l-call-to-action-6">
        <div class="call-sec-style py-5">
            <div class="container py-md-4 py-3">
                <div class="row align-items-center">
                    <div class="col-md-5 col-float-lt">
                        <h3 class="title-big">Contato</h3>
                        <p>Entre em contato conosco agora!</p>
                    </div>
                    <div class="col-md-7 float-rt text-md-right align-items-center mt-md-0 mt-4">
                        <ul class="buttons">
                            <li class="phone-sec">
                                <i class="fas fa-phone-volume mr-1"></i>
                                <a class="call-style-w3" href="tel:+55{{ $contato }}">{{ $settings['contato'] }}</a>
                            </li>
                            <li class="green">Ou</li>
                            <li><a href="{{ route('contacts') }}" class="btn btn-style-white btn-style-primary">Formulário</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endisset
<!-- //call section -->

<!-- footer -->
<section class="w3l-footer-29-main">
    <div class="footer-29 py-5">
        <div class="container py-lg-4">
            <div class="row footer-top-29">
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-md-4 col-6 footer-list-29">
                            <h6 class="footer-title-29">Empresa</h6>
                            <ul>
                                @foreach ($header as $item)
                                    <li><a href="{{ route($item['route']) }}">{{ $item['title'] }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-md-8 col-6 footer-list-29">
                            <h6 class="footer-title-29">Treinamentos</h6>
                            <ul>
                                @foreach ($treinamentos->lazy() as $item)
                                   <li><a href="{{ route('training',['slug' => $item->slug]) }}">{{ $item->title }}</a></li> 
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 footer-contact-list mt-lg-0 mt-md-5 mt-4">
                    <h6 class="footer-title-29">Contatos</h6>
                    <ul>
                        @isset($whatslink)
                            <li class="d-flex align-items-center py-2">
                                <i class="fab fa-whatsapp mr-2"></i>
                                <a href="https://api.whatsapp.com/send?phone=+55{{ $whatslink }}&text=oi">{{ $settings['whatsapp'] }}</a>
                            </li>
                        @endisset
                        @isset($contato)
                            <li class="d-flex align-items-center py-2">
                                <i class="fa fa-phone mr-2" aria-hidden="true"></i>
                                <a href="tel:+55{{ $contato }}">{{ $settings['contato'] }}</a>
                            </li>
                        @endisset
                        @isset($settings['email-contato'])
                            <li class="d-flex align-items-center py-2">
                                <i class="fa fa-envelope mr-2" aria-hidden="true"></i>
                                <a href="mailto:{{ $settings['email-contato'] }}">{{ $settings['email-contato'] }}</a>
                            </li>
                        @endisset
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- //footer -->
<!-- copyright -->
<section class="w3l-copyright">
    <div class="container">
        <div class="row bottom-copies">
            <p class="col-lg-8 copy-footer-29">© {{ now()->format('Y') }} San Martin Inteligência Organizacional |  Desenvolvido por <a href="https://argilatech.com.br" target="_blank" rel="noopener noreferrer">ArgilaTech</a></p>
            <div class="col-lg-4 text-right">
                <div class="main-social-footer-29">
                    @isset($settings['facebook'])
                        <a href="{{ $settings['facebook'] }}" class="facebook" target="_blank" rel="noopener noreferrer">
                            <i class="fab fa-facebook-f mt-2"></i>
                        </a>
                    @endisset
                    @isset($settings['instagram'])
                       <a href="{{ $settings['instagram'] }}" class="facebook" target="_blank" rel="noopener noreferrer">
                            <i class="fab fa-instagram mt-2"></i>
                        </a>
                    @endisset
                    @isset($settings['linkedin'])
                        <a href="{{ $settings['linkedin'] }}" class="facebook" target="_blank" rel="noopener noreferrer">
                            <i class="fab fa-linkedin-in mt-2"></i>
                        </a>
                    @endisset
                </div>
            </div>
        </div>
    </div>
</section>
<button onclick="topFunction()" id="movetop" title="Go to top">
    <i class="fas fa-level-up-alt"></i>
</button>