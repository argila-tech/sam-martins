<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/js/all.min.js" integrity="sha512-Tn2m0TIpgVyTzzvmxLNuqbSJH3JP8jm+Cy3hvHrW7ndTDcJ1w5mBiksqDBb8GpE2ksktFvDB/ykZ0mDpsZj20w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('js/theme-change.js') }}"></script>
{{--  <script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>  --}}
<script src="{{ asset('js/counter.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/site.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://www.google.com/recaptcha/api.js?render=6LeC0ScdAAAAAFzAj1N9lpLKPagJg6fwOa3k8Vu8"></script>
<script>
    @if(session('success'))
        Swal.fire(
            'Obrigado!',
            '{!! session('success') !!}',
            'success'
            
        )
    @endif

    @if(session('error'))
        Swal.fire(
            'Descupe!',
            '{!! session('error') !!}',
            'error',
        )
    @endif
    @if(session('warning'))
        Swal.fire(
            'Ops!',
            '{!! session('warning') !!}',
            'warning',
        )
    @endif
</script>

{{ @$js }}