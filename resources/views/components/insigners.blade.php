<!-- stats -->
<section class="w3_stats py-5" id="stats">
    <div class="container py-md-5 py-4">
        <div class="w3-stats text-center py-md-4">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="section-heading text-center mb-sm-5 mb-4">
                        <h3 class="title-style text-white mb-2">Our Amazing Statistics</h3>
                        <p class="lead text-white">
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque,
                            eaque ipsa quae ab illo inventore.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row mt-4">
                @for ($i = 0; $i < 5; $i++)
                    <div class="col-md-3 col-6">
                        <div class="counter">
                            <span class="fa fa-users"></span>
                            <div class="timer count-title count-number mt-3" data-to="1286" data-speed="1500"></div>
                            <p class="count-text">Employees</p>
                        </div>
                    </div>
                @endfor
            </div>
        </div>
    </div>
</section>
<!-- //stats -->