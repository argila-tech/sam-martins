<!-- customers section -->
<section class="w3l-customer-block py-5">
    <div class="container py-md-5 py-4">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="section-heading text-center mb-sm-5 mb-4">
                    <h3 class="title-style mb-2">Depoimentos</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="owl-carousel owl-theme" id="testimonials-carousel">
                @foreach ($items->lazy() as $item)
                    <div class="item mt-md-0 mt-5 py-5">
                        <div class="customer-single pr-lg-4">
                            <blockquote>{!! $item->description !!}</blockquote>
                            <div class="customer-img d-flex align-items-center mt-4">
                                <div style="width: 70px; display: flex; justify-content: center;margin-right: 15px;">
                                    <div style="background-image: url('{{ asset(@$item->files->path) }}'); width: 60px; height: 60px; background-size: cover; background-position: center; border-radius: 100%;">
                                        <img src="{{ asset(@$item->files->path) }}" class="d-none" alt="{{ $item->office }}" />
                                    </div>
                                </div>
                                <div class="customer-info ml-3">
                                    <h6>{{ $item->title }}</h6>
                                    <small class="mb-0">{{ $item->office }}</small>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
<!-- //customers section -->