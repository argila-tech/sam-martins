<!-- blog section -->
<div class="w3l-grids-block-5 py-5">
    <div class="container py-md-5 py-4">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="section-heading text-center mb-sm-5 mb-4">
                    <h3 class="title-style mb-2">Nosso Blog</h3>
                    <p class="lead">
                        Fique por dentro das ultimas notícias do mundo da Alta Gestão
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach ($items->lazy() as $item)
                <div class="col-lg-4 col-sm-6 mt-sm-0 mt-4">
                    <div class="blog-card-single">
                        <div class="grids5-info">
                            <img src="{{ asset($item->files->path) }}" class="img-fluid w-100" alt="{{ $item->title }}" />
                            <div class="blog-info">
                                <h5>November 6, 2020 - <a href="blog-single">New</a></h5>
                                <h4><a href="{{ route('blog',['slug' => $item->slug]) }}">{{ $item->title }}</a></h4>
                                <p>{!! Str::limit(strip_tags($item->description), 100, '...') !!}</p>
                            </div>
                        </div>
                        {{-- <div class="card-footer">
                            <ul class="admin-list d-flex align-items-center justify-content-between">
                                <li><a href="#blog"><span class="fa fa-user-circle"
                                            aria-hidden="true"></span>
                                        Admin</a></li>
                                <li><a href="#blog"><span class="fa fa-comments" aria-hidden="true"></span>3
                                        Comments</a>
                                </li>
                            </ul>
                        </div> --}}
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
<!-- //blog section -->