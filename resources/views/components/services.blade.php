<!-- services section -->
<section class="w3l-content-11-main">
    <div class="content-design-11 py-5">
        <div class="container py-md-5 py-4">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="section-heading text-center mb-sm-5 mb-4">
                        <h3 class="title-style mb-2">Nossos Serviços</h3>
                        <p class="lead">
                            Tudo é realizado através de uma metodologia exclusiva, que potencializa o processo de mudança 
                            e desenvolvimento dos participantes. Veja os serviços que podemos oferecer pra você e sua Empresa:
                        </p>
                    </div>
                </div>
            </div>
            <div class="content-sec-11">
                <div class="row">
                    <div class="owl-carousel owl-theme" id="service-carousel">
                        @foreach ($items->lazy() as $item)
                            <div class="item">
                                <div class="services-single d-flex p-sm-5 p-4">
                                    <div class="service-icon mr-sm-4 mr-3">
                                        {!! $item->icon !!}
                                        {{-- {!! Str::replace('i','span',$item->icon) !!} --}}
                                        {{-- <span class="fa fa-headphones" aria-hidden="true"></span> --}}
                                    </div>
                                    <div class="services-content">
                                        <h5><a href="{{ route('service',['slug' => $item->slug]) }}">{{ $item->title }}</a></h5>
                                        <p>{!! Str::limit($item->description, 200) !!}</p>
                                        {{-- <p>{!! $item->description !!}</p> --}}
                                         <a href="{{ route('service',['slug' => $item->slug]) }}" class="btn read-button d-flex align-items-center mt-4 p-0">Saiba mais<i class="fa fa-angle-double-right ml-1" aria-hidden="true"></i></a> 
                                    </div>
                                </div>
                            </div> 
                        @endforeach
                    </div>
                </div>
                <div class="mt-md-5 mt-4 text-center">
                    <a class="btn btn-style-white btn-style-primary" href="{{ route('services') }}">Todos os Serviços</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- //services section -->