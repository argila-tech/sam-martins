<!DOCTYPE html>
<html lang="en">
    <head>
        @isset($settings['google-analytics-head'])
            {!! $settings['google-analytics-head'] !!}
        @endisset
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <meta name="robots" content="index,follow,noodp" />
        <meta name="googlebot" content="index,follow" />
        @if (isset($head) && !empty($head))
            {{ @$head }}
        @else
            @isset($settings['palavras-chaves'])
                <meta name="keywords" content="{{ $settings['palavras-chaves'] }}" />
            @endisset
            @isset($settings['descricao-do-site'])
                <meta name="description" content="{{ $settings['descricao-do-site'] }}" />
                <meta name="subjet" content="{{ $settings['descricao-do-site'] }}" />
                <meta name="abstract" content="{{ $settings['descricao-do-site'] }}" />
                <meta name="topic" content="{{ $settings['descricao-do-site'] }}" />
                <meta name="summary" content="{{ $settings['descricao-do-site'] }}" />
                
                <meta property="og:description" content="{{ $settings['descricao-do-site'] }}" />
                <meta itemprop="description" content="{{ $settings['descricao-do-site'] }}" />
                <meta name="twitter:description" content="{{ $settings['descricao-do-site'] }}" />
            @endisset
            
            <meta property="og:url" content="{{ request()->fullUrl() }}" />
            <meta property="og:type" content="website" />
            <meta property="og:title" content="San Martin Inteligência Organizacional{{ @$title ? ' - '.@$title : ''  }}" /> 
            <meta property="og:image" content="{{ asset('assets/img/icon.png') }}" />
            
            <meta property="og:site_name" content="San Martin Inteligência Organizacional{{ @$title ? ' - '.@$title : ''  }}" />
            <meta property="og:locale" content="pt_BR" />
            <meta itemprop="name" content="San Martin Inteligência Organizacional" />
            
            <meta itemprop="image" content="{{ asset('assets/img/icon.png') }}" />
            <meta name="twitter:card" content="summary" />
            <meta name="twitter:site" content="San Martin Inteligência Organizacional{{ @$title ? ' - '.@$title : ''  }}" />
            <meta name="twitter:url" content="{{ request()->fullUrl() }}" />
            <meta name="twitter:title" content="San Martin Inteligência Organizacional{{ @$title ? ' - '.@$title : ''  }}" />
            <meta name="twitter:image" content="{{ asset('assets/img/icon.png') }}" />
        @endif

        <title>San Martin Inteligência Organizacional{{ @$title ? ' - '.@$title : ''  }}</title>
        <link rel="author" href="Argila Tech" />
        <link rel="me" href="https://www.argilatech.com.br" type="text/html" />
        <link rel="me" href="mailto:luiz.ferreira@argilatech.com.br" />
        <link rel="me" href="sms:+5584991956348" />


        <link href="//fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        
        {{ @$css }}

    </head>
    <body>

        @isset($settings['google-analytics-boby'])
            {!! $settings['google-analytics-boby'] !!}
        @endisset

        <x-Header :items='$header'/>
        {{ $content }}
        <x-Footer />
        <x-scripts />

        @isset($settings['whatsapp'])
            <a class="position-fixed" style="right: 10px; z-index: 9;bottom: 155px; z-index: 9; cursor: pointer;" href="https://api.whatsapp.com/send?phone=+55{{ $whatslink }}&text=oi" target="_blank" rel="noopener noreferrer">
                <img src="{{ asset('assets/img/whatsapp.png') }}" alt="Whatsapp" style="width: 50px;">
            </a>
        @endisset
        
    </body>
</html>