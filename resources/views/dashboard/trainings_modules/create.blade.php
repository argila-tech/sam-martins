<x-dashboard>
    <x-slot name="title">{{ __('Modules do Treinamentos: '. $item->title .' / Adicionar') }}</x-slot>
    <x-slot name="header">
        {{-- @include('layouts.headers.cards') --}}
    </x-slot>
    <x-slot name="content"> 
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="card w-100">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <div class="row">
                            <div class="col-8">
                                <h3 class="mb-0">Informações do módulo</h3>
                            </div>
                            <div class="col-4 d-flex justify-content-end">
                                <a href="javascript:history.back();" class="btn btn-danger btn-sm">Voltar</a>
                            </div>
                        </div>
                    </div>
                    <x-form action="{{ route('dashboard.trainings.modules.store',['training' => $item]) }}">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-lg-8 form-group">
                                    <x-label for="Título" />
                                    <x-input name="title" class="form-control" value="{{ old('title') }}" />
                                    <x-error field="title" class="text-danger" />
                                </div>
                                <div class="col-12 col-lg-4 form-group">
                                    <x-label for="ativo" />
                                    <select name="active" class="form-control">
                                        <option value="1" {{ old('active') == 1 ? 'selected' : '' }}>Sim</option>
                                        <option value="0" {{ old('active') == 0 ? 'selected' : '' }}>Não</option>
                                    </select>
                                    <x-error field="active" class="text-danger" />
                                </div>
                                <div class="col-12 col-lg-6 form-group">
                                    <x-label for="Inicio" />
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="ni ni-calendar-grid-58"></i>
                                            </span>
                                        </div>
                                        <x-input class="form-control" placeholder="Inicio" name="start_at"
                                            type="date" value="{{ old('start_at') }}" />
                                    </div>
                                    <x-error field="start_at" class="text-danger" />
                                </div>
                                <div class="col-12 col-lg-6 form-group">
                                    <x-label for="Instrutor" />
                                    <select name="teams_id" class="form-control">
                                        @foreach ($teams->lazy() as $team)
                                            <option value="{{ $team->id }} {{ $team->id == old('teams_id') ? 'seleted' : '' }}">
                                                {{ $team->title }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <x-error field="teams_id" class="text-danger" />
                                </div>
                                <div class="col-12 form-group">
                                    <x-label for="Descrição" />
                                    <x-textarea name="description" class="form-control" id="content">{{ old('description') }}</x-textarea>
                                    <x-error field="description" class="text-danger" />
                                </div>
                            </div>
                        </div>
                        <!-- Card footer -->
                        <div class="card-footer py-4">
                            <nav class="d-flex justify-content-end" aria-label="...">
                                <button type="submit" class="btn btn-success">Cadastrar</button>
                            </nav>
                        </div>
                    </x-form>
                </div>
            </div>
        </div>
    </x-slot>
</x-dashboard>