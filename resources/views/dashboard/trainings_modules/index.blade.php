<x-dashboard>
    <x-slot name="title">{{ __('Modules do Treinamentos: '. $item->title) }}</x-slot>
    <x-slot name="header">
        {{-- @include('layouts.headers.cards') --}}
        {{--  {{ dd($item) }}  --}}
    </x-slot>
    <x-slot name="content"> 
        <div class="container-fluid mt--7">
            <div class="row">
                @livewire('trainings-modules',['items' => $item->id])
            </div>
            <div class="row mt-5">

            </div>
        </div>
    </x-slot>
</x-dashboard>