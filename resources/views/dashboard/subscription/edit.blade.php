<x-dashboard>
    <x-slot name="title">{{ __('Treinamentos / Editar / '.$item->title) }}</x-slot>
    <x-slot name="header">
        {{-- @include('layouts.headers.cards') --}}
    </x-slot>
    <x-slot name="content"> 
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="card w-100">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <div class="row">
                            <div class="col-8">
                                <h3 class="mb-0">Editar Treinamento</h3>
                            </div>
                            <div class="col-4 d-flex justify-content-end">
                                <a href="javascript:history.back();" class="btn btn-danger btn-sm">Voltar</a>
                            </div>
                        </div>
                    </div>
                    <x-form action="{{ route('dashboard.trainings.update',$item) }}" has-files>
                        @method('put')
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-lg-8">
                                    <div class="form-group">
                                        <x-label for="Título" />
                                        <x-input name="title" class="form-control" value="{{ $item->title }}" />
                                        <x-error field="title" class="text-danger" />
                                    </div>
                                    <div class="form-group">
                                        <x-label for="Subtitulo" />
                                        <x-input name="subtitle" class="form-control" value="{{ $item->subtitle }}" />
                                        <x-error field="title" class="text-danger" />
                                    </div>
                                    <div class="form-group">
                                        <x-label for="Palavras Chaves" /> <strong><small class="text-dark">(Para indexação no google)</small></strong>
                                        <x-input name="tags" class="form-control" value="{{ $item->tags }}" />
                                        <x-error field="tags" class="text-danger" />
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4">
                                    <div class="form-group">
                                        <x-label for="Imagem" /><br>
                                        <x-input name="file" type="file" id="input-image"/>
                                        <img id="imagem" class="img-fluid" src="{{ asset($item->files->path) }}">
                                        <x-error field="file" class="text-danger" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <hr>
                                <div class="form-group">
                                    <h2>Investimento</h2>
                                    <div class="form-group row">
                                        <div class="form-group col-lg-6">
                                            <x-label for="Valor" />
                                            <x-input name="price" class="form-control money" value="{{ old('price',$item->price) }}" />
                                            <x-error field="price" class="text-danger" />
                                        </div>
                                        <div class="form-group col-lg-6">
                                            <x-label for="Parcelas" />
                                            <x-input name="installments" class="form-control" value="{{ old('installments',$item->installments) }}" />
                                            <x-error field="installments" class="text-danger" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <hr>
                                <div class="form-group">
                                    <h2>Datas e sobre o treinamento</h2>
                                </div>
                                <div class="form-group row">
                                    <div class="form-group col-lg-6">
                                        <x-label for="Inicio" />
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="ni ni-calendar-grid-58"></i>
                                                </span>
                                            </div>
                                            <x-input class="form-control" placeholder="Inicio" name="init_at"
                                                type="date" value="{{ $item->init_at }}" />
                                        </div>
                                        <x-error field="init_at" class="text-danger" />
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <x-label for="Fim" />
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="ni ni-calendar-grid-58"></i>
                                                </span>
                                            </div>
                                            <x-input class="form-control" placeholder="Inicio" name="finish_at"
                                                type="date" value="{{ $item->finish_at }}" />
                                        </div>
                                        <x-error field="finish_at" class="text-danger" />
                                        </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="form-group col-12 col-lg-6">
                                            <x-label for="Proxíma Turma" />
                                            <x-input class="form-control" name="next" value="{{ $item->next }}" />
                                            <x-error field="next" class="text-danger" />
                                        </div>
                                        <div class="form-group col-12 col-lg-6">
                                            <x-label for="ativo" />
                                            <select name="active" class="form-control">
                                                <option value="1" {{ $item->active == 1 ? 'selected' : '' }}>Sim</option>
                                                <option value="0" {{ $item->active == 0 ? 'selected' : '' }}>Não</option>
                                            </select>
                                            <x-error field="active" class="text-danger" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <x-label for="Conteúdo" />
                                    <x-textarea name="description" class="form-control" id="content">{{ $item->description }}</x-textarea>
                                    <x-error field="description" class="text-danger" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <hr>
                                    <h2>Localização do treinamento</h2>
                                </div>
                                <div class="form-group">                                    
                                    <x-label for="Inicio" />
                                    <x-input class="form-control" placeholder="Endereço" name="address" value="{{ $item->address }}" />
                                    <x-error field="address" class="text-danger" />
                                </div>
                                <div class="form-group">                                    
                                    <x-label for="Map" />
                                    <textarea class="form-control" placeholder="Mapa" name="map">{{ $item->map }}</textarea>
                                    <x-error field="map" class="text-danger" />
                                </div>
                            </div>
                        </div>
                        <!-- Card footer -->
                        <div class="card-footer py-4">
                            <nav class="d-flex justify-content-end" aria-label="...">
                                <button type="submit" class="btn btn-success">Atualizar</button>
                            </nav>
                        </div>
                    </x-form>
                </div>
            </div>
        </div>
    </x-slot>
</x-dashboard>