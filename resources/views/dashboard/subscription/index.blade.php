<x-dashboard>
    @if ($item != null)
        <x-slot name="title">{{ __('Pré inscritos de Treinamento: '.$item->title) }}</x-slot>
    @else
        <x-slot name="title">{{ __('Pré inscritos de Treinamentos') }}</x-slot>
    @endif
   
    <x-slot name="header">
        {{-- @include('layouts.headers.cards') --}}
    </x-slot>
    <x-slot name="content"> 
        <div class="container-fluid mt--7">
            <div class="row">
                @if ($item != null)
                    @livewire('subscription-list',['id' => $item->id])
                @else
                    @livewire('subscription')
                @endif
                
            </div>
            <div class="row mt-5">

            </div>
        </div>
    </x-slot>
</x-dashboard>