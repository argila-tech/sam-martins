<x-dashboard>
    <x-slot name="title">{{ __('Parceiros / Editar / '.$item->title) }}</x-slot>
    <x-slot name="header">
        {{-- @include('layouts.headers.cards') --}}
    </x-slot>
    <x-slot name="content"> 
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="card w-100">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <div class="row">
                            <div class="col-8">
                                <h3 class="mb-0">Editar</h3>
                            </div>
                            <div class="col-4 d-flex justify-content-end">
                                <a href="javascript:history.back();" class="btn btn-danger btn-sm">Voltar</a>
                            </div>
                        </div>
                    </div>
                    <x-form action="{{ route('dashboard.partners.update',$item) }}" has-files>
                        @method('put')
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-lg-8">
                                    <div class="form-group">
                                        <x-label for="Title" />
                                        <x-input name="title" class="form-control" value="{{ $item->title }}" />
                                        <x-error field="title" class="text-danger" />
                                    </div>
                                    <div class="form-group">
                                        <x-label for="Site" />
                                        <x-input name="site" class="form-control" value="{{ $item->site }}" />
                                        <x-error field="site" class="text-danger" />
                                    </div>
                                    <div class="form-group">
                                        <x-label for="Instagram" />
                                        <x-input name="instagram" class="form-control" value="{{ $item->instagram }}" />
                                        <x-error field="instagram" class="text-danger" />
                                    </div>
                                    <div class="form-group">
                                        <x-label for="Facebook" />
                                        <x-input name="facebook" class="form-control" value="{{ $item->facebook }}" />
                                        <x-error field="facebook" class="text-danger" />
                                    </div>
                                    <div class="form-group">
                                        <x-label for="Whatsapp" />
                                        <x-input name="whatsapp" class="form-control" value="{{ $item->whatsapp }}" />
                                        <x-error field="whatsapp" class="text-danger" />
                                    </div>
                                    <div class="form-group">
                                        <x-label for="Conteúdo" />
                                        <x-textarea name="description" class="form-control" id="content">{{ $item->description }}</x-textarea>
                                        <x-error field="description" class="text-danger" />
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4">
                                    <div class="form-group d-flex flex-column">
                                        <x-label for="Imagem" />
                                        <x-input name="file" type="file" id="input-image"/>
                                        <img id="imagem" class="img-fluid" src="{{ $item->files ? asset($item->files->path) : null }}">
                                        <x-error field="file" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Card footer -->
                        <div class="card-footer py-4">
                            <nav class="d-flex justify-content-end" aria-label="...">
                                <button type="submit" class="btn btn-primary">Atualizar</button>
                            </nav>
                        </div>
                    </x-form>
                </div>
            </div>
        </div>
    </x-slot>
</x-dashboard>