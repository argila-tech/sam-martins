<x-dashboard>
    <x-slot name="title">{{ __('Categorias de Treinamentos / '. $item->title.' / Editar') }}</x-slot>
    <x-slot name="header">
        {{-- @include('layouts.headers.cards') --}}
    </x-slot>
    <x-slot name="content"> 
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="card w-100">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <div class="row">
                            <div class="col-8">
                                <h3 class="mb-0">Informações da categoria</h3>
                            </div>
                            <div class="col-4 d-flex justify-content-end">
                                <a href="javascript:history.back();" class="btn btn-danger btn-sm">Voltar</a>
                            </div>
                        </div>
                    </div>
                    <x-form action="{{ route('dashboard.categorys.update', $item) }}">
                        @csrf
                        @method('put')
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-lg-8 form-group">
                                    <x-label for="Título" />
                                    <x-input name="title" class="form-control" value="{{ $item->title }}" />
                                    <x-error field="title" class="text-danger" />
                                </div>
                                <div class="col-12 col-lg-4 form-group">
                                    <x-label for="ativo" />
                                    <select name="active" class="form-control">
                                        <option value="1" {{ $item->active == 1 ? 'selected' : '' }}>Sim</option>
                                        <option value="0" {{ $item->active == 0 ? 'selected' : '' }}>Não</option>
                                    </select>
                                    <x-error field="active" class="text-danger" />
                                </div>
                            </div>
                        </div>
                        <!-- Card footer -->
                        <div class="card-footer py-4">
                            <nav class="d-flex justify-content-end" aria-label="...">
                                <button type="submit" class="btn btn-success">Atualizar</button>
                            </nav>
                        </div>
                    </x-form>
                </div>
            </div>
        </div>
    </x-slot>
</x-dashboard>