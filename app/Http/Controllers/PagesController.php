<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactsRequest;
use App\Http\Requests\TrainingsSubcriptionsRequest;
use App\Mail\Contacts as MailContacts;
use App\Mail\TrainingsSubscriptions;
use App\Models\Blogs;
use App\Models\Clients;
use App\Models\Contacts;
use App\Models\Pages;
use App\Models\Partners;
use App\Models\Services;
use App\Models\Settings;
use App\Models\Slides;
use App\Models\Teams;
use App\Models\Testimonials;
use App\Models\Trainings;
use App\Models\TrainingsSubscription;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Spatie\Sitemap\SitemapGenerator;

class PagesController extends Controller
{
    public $settings;
    public function __construct()
    {
        $this->settings = Settings::select('description','slug')->pluck('description','slug')->all();
        $header = [
            [
                'title' => 'Sobre',
                'route' => 'about'
            ],
            [
                'title' => 'Serviços',
                'route' => 'services'
            ],
            [
                'title' => 'Treinamentos',
                'route' => 'trainings'
            ],
            [
                'title' => 'Depoimentos',
                'route' => 'testimonials'
            ],
            [
                'title' => 'Parceiros',
                'route' => 'partners'
            ],
            [
                'title' => 'Clientes',
                'route' => 'clients'
            ],
            [
                'title' => 'Blog',
                'route' => 'blogs'
            ],
            [
                'title' => 'Contato',
                'route' => 'contacts'
            ],
            
        ];

        if($this->settings['whatsapp']):
            $numberWhats = preg_replace('/(\D+)/', '', $this->settings['whatsapp']);
            $numberWhats = preg_replace('/ˆ0?(\d{2})9?(\d{8})/', '$1$2', $numberWhats);
            view()->share('whatslink', $numberWhats);
        endif;

        if($this->settings['contato']):
            $numberWhats = preg_replace('/(\D+)/', '', $this->settings['contato']);
            $numberWhats = preg_replace('/ˆ0?(\d{2})9?(\d{8})/', '$1$2', $numberWhats);
            view()->share('contato', $numberWhats);
        endif;
        
        view()->share('settings', $this->settings);
        view()->share('header', $header);
        view()->share('treinamentos', Trainings::where('active', true)->select('title','slug')->inRandomOrder()->limit(7)->get());
    }
    
    public function index()
    {
        $slides = Slides::where(
                 [['active', 1], ['start_at', '<=', now()->format('Y-m-d')], ['finish_at', '>=', now()->format('Y-m-d')]]
            )->orderBy('position','asc')->get();

        return view('pages.index',[
            'slides' => $slides,
            'services' => Services::inRandomOrder()->limit(5)->get(),
            'team' => Teams::first(),
            'testimonials' => Testimonials::inRandomOrder()->limit(5)->get(),
            'blogs' => Blogs::where([
                ['active', true], ['publish_at', '<=', now()->format('Y-m-d')]
            ])->orderBy('created_at','asc')->limit(3)->get(),
            'about' => Pages::where('slug','sobre-index')->first()
        ]);
    }

    public function about()
    {
        return view('pages.about',[
            'blogs' => Blogs::where([
                ['active', true], ['publish_at', '>=', now()->format('Y-m-d')]
            ])->orderBy('created_at','asc')->limit(3)->get(),
            'item' => Pages::where('slug','somos-a-san-martin')->first(),
            'frase' => Pages::where('slug','frase-sobre')->first(),
            'testimonials' => Testimonials::inRandomOrder()->get()
        ]);
    }

    public function services()
    {
        return view('pages.services',[
            'items' => Services::simplepaginate(15)
        ]);
    }

    public function service($slug)
    {
        // dd(Services::where('slug','!=',$slug)->get());
        return view('pages.service',[
            'item' => Services::where('slug',$slug)->first(),
            'items' => Services::where('slug','!=',$slug)->inRandomOrder()->limit(4)->get()
        ]);
    }

    public function trainings()
    {
        return view('pages.trainings',[
            'items' => Trainings::where('active', true)->simplepaginate(10)
        ]);
    }

    public function training($slug)
    {
        $item = Trainings::where([['slug',$slug],['active', true]])->first();

        if(is_null($item))
            return abort(404, 'Treinamento não encontrada');

        return view('pages.training',[
            'item' => $item,
            'items' => Trainings::where([['slug','!=', $slug],['active', true]])->orderBy('created_at','asc')->limit(4)->get()
        ]);
    }

    public function sendTrainings(TrainingsSubcriptionsRequest $request)
    {
        // $sub = TrainingsSubscription::where([
        //     ['email', $request->email],
        //     ['trainings_id', $request->trainings_id]
        // ])->get();

        // dd($sub);

        // if($sub->count() > 0)
        //     return back()->with('warning','Você já é um pré inscrito. Em breve entraremos em contato.');

        $item = Trainings::find($request->trainings_id);
        
        $subtraining = new TrainingsSubscription;
        $subtraining->trainings_id = $request->trainings_id;
        $subtraining->name = $request->name;
        $subtraining->email = $request->email;
        $subtraining->phone = $request->phone;
        
        try {
            $subtraining->save();
        } catch (\Throwable $th) {
            //throw $th;
            Log::debug($th);
            return back()->with('error','Erro interno. Por favor tente novemente.');
        } finally {
            Mail::to($this->settings['email-contato'])->send(new TrainingsSubscriptions($item, $subtraining));
        }

        return back()->with('success','Obrigado! Em breve entraremos em contato.');
    }

    public function testimonials()
    {
        return view('pages.testimonials',[
            'items' => Testimonials::orderBy('created_at','asc')->inRandomOrder()->simplepaginate(9)
        ]);
    }

    public function clients()
    {
        return view('pages.clients',[
            'items' => Clients::simplepaginate(20)
        ]);
    }

    public function blogs()
    {
        return view('pages.blogs', [
            'items' => Blogs::orderBy('created_at','asc')->simplepaginate(20)
        ]);
    }

    public function blog($slug)
    {
        $blog = Blogs::where([
            ['slug',$slug],['active', true], ['publish_at', '<=', now()->format('Y-m-d')]
        ])->first();

        if(is_null($blog))
            return abort(404, 'Notícia não encontrada');

        return view('pages.blog', [
            'item' => $blog,
            'items' => Blogs::where([['slug','!=',$slug],['active', true], ['publish_at', '<=', now()->format('Y-m-d')]])->orderBy('created_at','asc')->limit(3)->get()
        ]);
    }

    public function contacts($slug = null)
    {
        if($slug != null)
            $slug = Services::where('slug',$slug)->select('title')->first()->title;

        return view('pages.contacts',[
            'assunto' => $slug
        ]);
    }

    public function sendContacts(ContactsRequest $request)
    {
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $data = [
            'secret' => '6LeC0ScdAAAAAKkmKvUpKbKaREZ1aU6Kz4ydtfy8',
            'response' => $request->recaptcha
        ];
        
        $options = [
            'http'=>[
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($data)
            ]
        ];
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        $resultJson = json_decode($result);

    
        if($resultJson->success == false){
            return back()->with('error','Error na verificação do Recaptcha. Recarregue a página e tente novamente.');
        }
            
        $contact = new Contacts;
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->phone = $request->phone;
        $contact->message = $request->message;
        $request->subject ? $contact->subject = $request->subject : '';
        
        try {
            $contact->save();
        } catch (\Throwable $th) {
            throw $th;
            return back()->with('error','Erro ao enviar. Por favor tente novamente.');
        } finally {
            Mail::to($this->settings['email-contato'])->send(new MailContacts($contact->id));
        }

        return back()->with('success','Em breve entraremos em contato.');
    }

    public function siteMap()
    {
        return SitemapGenerator::create('https://portalsmn.com.br')
                ->getSitemap()
                     ->writeToFile(public_path('sitemap.xml'));
    }

    public function partners()
    {
        return view('pages.partners',[
            'items' => Partners::orderBy('created_at','asc')->simplepaginate(15)
        ]);
    }

    public function partner($slug)
    {
        return view('pages.partner',[
            'item' => Partners::where('slug',$slug)->first(),
            'items' => Partners::where('slug','!=', $slug)->orderBy('created_at','asc')->limit(3)->get()
        ]);
    }
}