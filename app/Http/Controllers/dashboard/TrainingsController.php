<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\TrainingsRequest;
use App\Models\Files;
use App\Models\Trainings;
use App\Models\TrainingsCategorys;
// use Carbon\Carbon;

class TrainingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.trainings.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.trainings.create',[
            'categorys' => TrainingsCategorys::orderBy('title','asc')->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TrainingsRequest $request)
    {
        $file  = new Files;
        $file->Upload($file, $request->file);
        $request['files_id'] = $file->id;
        $request['price'] = (float)str_replace(',','.',str_replace('.','',$request->price));
        // $request['init_at'] = Carbon::createFromFormat('d/m/Y', $request->init_at);
        // $request['finish_at'] = Carbon::createFromFormat('d/m/Y', $request->finish_at);

        try {
            Trainings::create($request->all());
        } catch (\Throwable $th) {
            throw $th;
            return back()->with('error','Erro ao cadastrar treinamento.');
        }

        return redirect()->route('dashboard.trainings.index')->with('success','Treinamento cadastrado com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Trainings  $trainings
     * @return \Illuminate\Http\Response
     */
    public function show(Trainings $training)
    {
        return view('dashboard.trainings.show', ['item' => $training]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Trainings  $trainings
     * @return \Illuminate\Http\Response
     */
    public function edit(Trainings $training)
    {
        return view('dashboard.trainings.edit',[
            'item' => $training,
            'categorys' => TrainingsCategorys::orderBy('title','asc')->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Trainings  $trainings
     * @return \Illuminate\Http\Response
     */
    public function update(TrainingsRequest $request, Trainings $training)
    {
        if(!is_null($request->file)):
            $file  = new Files;
            $file->Upload($file, $request->file);
            $request['files_id'] = $file->id;
        endif;

        if(!is_null($request->price)):
            $request['price'] = (float)str_replace(',','.',str_replace('.','',$request->price));
        endif;

        // $request['init_at'] = Carbon::createFromFormat('d/m/Y', $request->init_at);
        // $request['finish_at'] = Carbon::createFromFormat('d/m/Y', $request->finish_at);

        try {
            $training->update($request->all());
        } catch (\Throwable $th) {
            throw $th;
            return back()->with('error','Erro ao atualizar treinamento.');
        }

        return redirect()->route('dashboard.trainings.index')->with('success','Treinamento atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Trainings  $trainings
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trainings $training)
    {
        try {
            $training->delete();
        } catch (\Throwable $th) {
            //throw $th;
            return back()->with('error','Erro ao apagar Treinamento,');
        }

        return redirect()->route('dashboard.trainings.index')->with('success','Treinamento apagado com sucesso.');
    }
}
