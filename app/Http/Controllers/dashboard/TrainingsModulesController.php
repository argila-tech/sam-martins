<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use App\Models\Teams;
use App\Models\Trainings;
use App\Models\TrainingsModules;
use Illuminate\Http\Request;

class TrainingsModulesController extends Controller
{
    private $module;

    public function __construct(TrainingsModules $module)
    {
        $this->module = $module;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Trainings $training)
    {
        // dd($training);
        return view('dashboard.trainings_modules.index',['item' => $training]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Trainings $training)
    {
        return view('dashboard.trainings_modules.create',[
            'item' => $training,
            'teams' => Teams::orderBy('title','asc')->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $training)
    {
        $request['trainings_id'] = $training;

        try {
            TrainingsModules::create($request->all());
        } catch (\Throwable $th) {
            throw $th;
            return back()->with('error','Erro ao cadastrar módulo. Tente novamente.');
        }

        return redirect()->route('dashboard.trainings.modules.index',['training' => $training])->with('success','Módulo cadastrado com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TrainingsModules  $trainingsModules
     * @return \Illuminate\Http\Response
     */
    public function show(TrainingsModules $trainingsModules)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TrainingsModules  $trainingsModules
     * @return \Illuminate\Http\Response
     */
    public function edit($training, $module)
    {
        return view('dashboard.trainings_modules.edit',[
            'item' => $this->module->find($module),
            'teams' => Teams::orderBy('title','asc')->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TrainingsModules  $trainingsModules
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $training, $module)
    {
        try {
           $this->module->find($module)->update($request->all());
        } catch (\Throwable $th) {
            // throw $th;
            return back()->with('error','Erro ao atualizar módulo. Tente novamente.');
        }

        return redirect()->route('dashboard.trainings.modules.index',['training'=> $training])->with('success','Módulo atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TrainingsModules  $trainingsModules
     * @return \Illuminate\Http\Response
     */
    public function destroy($training, $module)
    {
        try {
            $this->module->find($module)->delete();
        } catch (\Throwable $th) {
            //throw $th;
            return back()->with('error','Erro ao apagar módulo. Tente novamente.');
        }

        return back()->with('success','Módulo atualizado com sucesso.');
    }
}
