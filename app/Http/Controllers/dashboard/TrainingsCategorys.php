<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use App\Models\TrainingsCategorys as ModelTrainingsCategorys;
use Illuminate\Http\Request;

class TrainingsCategorys extends Controller
{
    private $category;

    public function __construct(ModelTrainingsCategorys $category)
    {
        $this->category = $category;    
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.trainings_category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.trainings_category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            ModelTrainingsCategorys::create($request->all());
        } catch (\Throwable $th) {
            //throw $th;
            return back()->with('error','Erro ao cadastrar categoria. Por favor tente novamente');
        }

        return redirect()->route('dashboard.categorys.index')->with('success','Categoria cadastrado com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TrainingsCategorys  $trainingsCategorys
     * @return \Illuminate\Http\Response
     */
    public function show(ModelTrainingsCategorys $trainingsCategorys)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TrainingsCategorys  $trainingsCategorys
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('dashboard.trainings_category.edit',['item' => $this->category->find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TrainingsCategorys  $trainingsCategorys
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->category->find($id)->update($request->all());
        } catch (\Throwable $th) {
            //throw $th;
            return back()->with('error','Erro ao atualizar categoria. Por favor tente novamente');
        }

        return redirect()->route('dashboard.categorys.index')->with('success','Categoria atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TrainingsCategorys  $trainingsCategorys
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->category->find($id)->delete();
        } catch (\Throwable $th) {
            //throw $th;
            return back()->with('error','Erro ao apagar categoria. Por favor tente novamente');
        }

        return redirect()->route('dashboard.categorys.index')->with('success','Categoria apagado com sucesso.');
    }
}
