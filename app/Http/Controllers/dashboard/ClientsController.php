<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use App\Models\Clients;
use App\Models\Files;
use Illuminate\Http\Request;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.clients.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = new Files;
        $file->Upload($file,$request->file);
        $request['files_id'] = $file->id;

        try {
            Clients::create($request->all());
        } catch (\Throwable $th) {
            throw $th;
            return back()->with('error','Erro ao cadastrar cliente.');
        }

        return redirect()->route('dashboard.clients.index')->with('success','cliente Cadastrado com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Clients  $clients
     * @return \Illuminate\Http\Response
     */
    public function show(Clients $clients)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Clients  $clients
     * @return \Illuminate\Http\Response
     */
    public function edit(Clients $client)
    {
        return view('dashboard.clients.edit',['item' => $client]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Clients  $clients
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Clients $client)
    {
        
        if($request->file):
            $file = new Files;
            $file->Upload($file,$request->file);
            $request['files_id'] = $file->id;
        endif;

        try {
            $client->update($request->all());
        } catch (\Throwable $th) {
            //throw $th;
            return back()->with('error','Erro ao cadastrar cliente.');
        }

        return redirect()->route('dashboard.clients.index')->with('success','cliente atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Clients  $clients
     * @return \Illuminate\Http\Response
     */
    public function destroy(Clients $client)
    {
        try {
            $client->delete();
        } catch (\Throwable $th) {
            throw $th;
            return back()->with('error','Erro ao cadastrar cliente.');
        }

        return redirect()->route('dashboard.clients.index')->with('success','cliente apagado com sucesso.');
    }
}
