<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\TeamsRequest;
use App\Models\Files;
use App\Models\Teams;

class TeamsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.teams.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.teams.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeamsRequest $request)
    {
        $file = new Files;
        $file->upload($file, $request->file);
        $request['files_id'] = $file->id;

        try {
            Teams::create($request->all());
        } catch (\Throwable $th) {
            //throw $th;
            return redirect()->back()->with('error','Erro ao cadastrar Time.');
        }

        return redirect()->route('dashboard.teams.index')->with('success','Time cadastrado com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Teams  $teams
     * @return \Illuminate\Http\Response
     */
    public function show(Teams $teams)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Teams  $teams
     * @return \Illuminate\Http\Response
     */
    public function edit(Teams $team)
    {
        return view('dashboard.teams.edit',['item' => $team]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Teams  $teams
     * @return \Illuminate\Http\Response
     */
    public function update(TeamsRequest $request, Teams $team)
    {
        if($request->file):
            $file = new Files;
            $file->upload($file, $request->file);
            $request['files_id'] = $file->id;
        endif;

        try {
            $team->update($request->all());
        } catch (\Throwable $th) {
            //throw $th;
            return redirect()->back()->with('error','Erro ao atualizar Time.');
        }

        return redirect()->route('dashboard.teams.index')->with('success','Time Atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Teams  $teams
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teams $team)
    {
        try {
            $team->delete();
        } catch (\Throwable $th) {
            //throw $th;
            return back()->with('error','Erro ao apagar Time.');
        }

        return redirect()->route('dashboard.teams.index')->with('success','Time apagado com sucesso.');
    }
}
