<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use App\Models\Trainings;
use App\Models\TrainingsSubscription;
use Illuminate\Http\Request;

class TrainigsSubscriptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = null;
        if(request()->id)
            $item = Trainings::find(request()->id);

        return view('dashboard.subscription.index',['item' => $item]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TrainingsSubscription  $trainingsSubscription
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = TrainingsSubscription::find($id);
        $item->read = 1;
        $item->save();
        
        return view('dashboard.subscription.show',['item' => $item]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TrainingsSubscription  $trainingsSubscription
     * @return \Illuminate\Http\Response
     */
    public function edit(TrainingsSubscription $trainingsSubscription)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TrainingsSubscription  $trainingsSubscription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TrainingsSubscription $trainingsSubscription)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TrainingsSubscription  $trainingsSubscription
     * @return \Illuminate\Http\Response
     */
    public function destroy(TrainingsSubscription $trainingsSubscription)
    {
        //
    }
}
