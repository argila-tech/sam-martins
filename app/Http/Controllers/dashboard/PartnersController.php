<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use App\Models\Files;
use App\Models\Partners;
use Illuminate\Http\Request;

class PartnersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.partners.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.partners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = new Files;

        try {
            $file->upload($file, $request->file);
        } catch (\Throwable $th) {
            // dd($th);
            return redirect()->back()->with('error','Erro ao cadastrar imagem!');
        }
        $request['files_id'] = $file->id; 

        try {
            Partners::create($request->all());
        } catch (\Throwable $th) {
            // dd($th);
            return redirect()->back()->with('error','Erro ao cadastrar parceiro!');
        }

        return redirect()->route('dashboard.partners.index')->with('success','Parceito criado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Partners  $partners
     * @return \Illuminate\Http\Response
     */
    public function show(Partners $partners)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Partners  $partners
     * @return \Illuminate\Http\Response
     */
    public function edit(Partners $partner)
    {
        return view('dashboard.partners.edit',['item' => $partner]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Partners  $partners
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Partners $partner)
    {
        if($request->file):
            $file = new Files;

            try {
                $file->upload($file, $request->file);
            } catch (\Throwable $th) {
                // throw $th;
                return redirect()->back()->with('error','Erro ao atualizar imagem!');
            }

            $request['files_id'] = $file->id;

        endif;

        try {
            $partner->update($request->all());
        } catch (\Throwable $th) {
            // throw $th;
            return redirect()->back()->with('error','Erro ao atualizar parceiro!');
        }

        return redirect()->route('dashboard.partners.index')->with('success','Parceiro Atualizado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Partners  $partners
     * @return \Illuminate\Http\Response
     */
    public function destroy(Partners $partner)
    {
        try {
            $partner->delete();
        } catch (\Throwable $th) {
            // throw $th;
            return redirect()->back()->with('error','Erro ao excluir parceiro!');
        }

        return redirect()->back()->with('success','Parceiro apagado com sucesso!');
    }
}
