<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use App\Models\Services;
use Illuminate\Http\Request;

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.services.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            Services::create($request->all());
        } catch (\Throwable $th) {
            //throw $th;
            return back()->with('error','Erro ao cadastrar serviço.');
        }

        return redirect()->route('dashboard.services.index')->with('success','Serviço cadastrado com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function show(Services $services)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function edit(Services $service)
    {
        return view('dashboard.services.edit',['item' => $service]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Services $service)
    {
        try {
            $service->update($request->all());
        } catch (\Throwable $th) {
            //throw $th;
            return back()->with('error','Erro ao atualizar serviço.');
        }

        return redirect()->route('dashboard.services.index')->with('success','Serviço atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function destroy(Services $service)
    {
        try {
            $service->delete();
        } catch (\Throwable $th) {
            //throw $th;
            return back()->with('error','Erro ao apagar serviço.');
        }

        return redirect()->route('dashboard.services.index')->with('success','Serviço apagado com sucesso.');
    }
}
