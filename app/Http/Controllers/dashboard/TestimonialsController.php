<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use App\Models\Files;
use App\Models\Testimonials;
use Illuminate\Http\Request;

class TestimonialsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.testimonials.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.testimonials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = new Files;
        $file->upload($file, $request->file);
        $request['files_id'] = $file->id;
        
        try {
            Testimonials::create($request->all());
        } catch (\Throwable $th) {
            //throw $th;
            return back()->with('error','Erro ao cadastrar Depoimento.');
        }

        return redirect()->route('dashboard.testimonials.index')->with('success','Depoimento Cadastrado com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Testimonials  $testimonials
     * @return \Illuminate\Http\Response
     */
    public function show(Testimonials $testimonials)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Testimonials  $testimonials
     * @return \Illuminate\Http\Response
     */
    public function edit(Testimonials $testimonial)
    {
        return view('dashboard.testimonials.edit',['item' => $testimonial]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Testimonials  $testimonials
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Testimonials $testimonial)
    {
        if($request->file):
            $file = new Files;
            $file->upload($file, $request->file);
            $request['files_id'] = $file->id;
        endif;

        try {
            $testimonial->update($request->all());
        } catch (\Throwable $th) {
            //throw $th;
            return back()->with('error','Erro ao atualizar Depoimento.');
        }

        return redirect()->route('dashboard.testimonials.index')->with('success','Depoimento atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Testimonials  $testimonials
     * @return \Illuminate\Http\Response
     */
    public function destroy(Testimonials $testimonial)
    {
         try {
            $testimonial->delete();
        } catch (\Throwable $th) {
            //throw $th;
            return back()->with('error','Erro ao apagar Depoimento.');
        }

        return redirect()->route('dashboard.testimonials.index')->with('success','Depoimento apagado com sucesso.');
    }
}
