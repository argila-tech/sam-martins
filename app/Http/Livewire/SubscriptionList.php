<?php

namespace App\Http\Livewire;

use App\Models\TrainingsSubscription;
use Livewire\Component;
use Livewire\WithPagination;

class SubscriptionList extends Component
{
    public $id, $search;
    protected $queryString = ['search'];
    protected $paginationTheme = 'bootstrap';


    public function __construct($id)
    {
        $this->id = $id;
    }

    public function render()
    {
        return view('livewire.subscription-list',[
            'items' => TrainingsSubscription::where(
                [
                    ['trainings_id', $this->id],
                    ['name', 'like', '%'.$this->search.'%'],
                ]
            )->orderBy('created_at','asc')->paginate(15)
        ]);
    }
}
