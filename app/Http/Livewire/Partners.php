<?php

namespace App\Http\Livewire;

use App\Models\Partners as ModelsPartners;
use Livewire\Component;
use Livewire\WithPagination;

class Partners extends Component
{
    use WithPagination;

    public $search;
    protected $queryString = ['search'];
    protected $paginationTheme = 'bootstrap';
    
    public function render()
    {
        return view('livewire.partners',[
            'items' => ModelsPartners::where('title', 'like', '%'.$this->search.'%')
                        ->orderBy('created_at','asc')
                            ->paginate(15)
        ]);
    }
}
