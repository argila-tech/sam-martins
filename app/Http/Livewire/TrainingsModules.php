<?php

namespace App\Http\Livewire;

use App\Models\TrainingsModules as ModelsTrainingsModules;
use Livewire\Component;
use Livewire\WithPagination;

class TrainingsModules extends Component
{
    use WithPagination;
    public $items, $search;

    protected $queryString = ['search'];
    protected $paginationTheme = 'bootstrap';

    public function render()
    {
        return view('livewire.trainings-modules',[
            'dados' => ModelsTrainingsModules::where(
                [
                    ['trainings_id', $this->items],
                    ['title', 'like', '%'.$this->search.'%'],
                ]
            )->orderBy('title','asc')->paginate(15),
            'idTrainings' => $this->items
        ]);
    }
}
