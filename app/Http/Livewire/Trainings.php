<?php

namespace App\Http\Livewire;

use App\Models\Trainings as ModelsTrainings;
use Livewire\Component;
use Livewire\WithPagination;

class Trainings extends Component
{
    use WithPagination;

    public $search;
    protected $queryString = ['search'];
    protected $paginationTheme = 'bootstrap';

    public function render()
    {
        return view('livewire.trainings',[
            'items' => ModelsTrainings::where('title', 'like', '%'.$this->search.'%')
                        ->orderBy('created_at','asc')
                            ->paginate(15)
        ]);
    }
}
