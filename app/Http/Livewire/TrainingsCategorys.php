<?php

namespace App\Http\Livewire;

use App\Models\TrainingsCategorys as ModelsTrainingsCategorys;
use Livewire\Component;
use Livewire\WithPagination;

class TrainingsCategorys extends Component
{
    use WithPagination;

    public $search;
    protected $queryString = ['search'];
    protected $paginationTheme = 'bootstrap';

    public function render()
    {
        return view('livewire.trainings-categorys',[
            'items' => ModelsTrainingsCategorys::where('title', 'like', '%'.$this->search.'%')
                        ->orderBy('created_at','asc')
                            ->paginate(15)
        ]);
    }
}
