<?php

namespace App\Http\Livewire;

use App\Models\Trainings;
use Livewire\Component;
use Livewire\WithPagination;

class Subscription extends Component
{
    use WithPagination;

    public $search;
    protected $queryString = ['search'];
    protected $paginationTheme = 'bootstrap';

    public function render()
    {
        return view('livewire.subscription',[
            'items' => Trainings::where('title', 'like', '%'.$this->search.'%')
                        ->orderBy('created_at','asc')
                            ->paginate(15)
        ]);
    }
}
