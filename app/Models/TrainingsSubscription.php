<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrainingsSubscription extends Model
{
    use SoftDeletes;

    protected $table = "trainings_subscriptions";
    protected $fillable = [
        'trainings_id',
        'name',
        'phone',
        'email'
    ];


    /**
     * Get the trainings that owns the TrainingsSubscription
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function trainings()
    {
        return $this->belongsTo(Trainings::class);
    }
}
