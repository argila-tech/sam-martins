<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Trainings extends Model
{
    use Sluggable, SoftDeletes;
    
    protected $table = 'trainings';
    protected $fillable = [
        'trainings_categorys_id',
        'title',
        'subtitle',
        'tags',
        'price',
        'installments',
        'init_at',
        'finish_at',
        'next',
        'active',
        'description',
        'address',
        'map',
        'files_id',
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * Get the Files that owns the Trainings
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Files()
    {
        return $this->belongsTo(Files::class);
    }

    /**
     * Get all of the subscriptions for the Trainings
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subscriptions()
    {
        return $this->hasMany(TrainingsSubscription::class);
    }

    /**
     * Get the categorys that owns the Trainings
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function categorys(): BelongsTo
    {
        return $this->belongsTo(TrainingsCategorys::class,'trainings_categorys_id');
    }

    /**
     * Get all of the modeles for the Trainings
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function modules(): HasMany
    {
        return $this->hasMany(TrainingsModules::class);
    }
}
