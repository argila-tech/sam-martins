<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Partners extends Model
{
    use Sluggable;

    protected $table = 'partners';
    protected $fillable = [
        'files_id',
        'title',
        'instagram',
        'site',
        'facebook',
        'whatsapp',
        'description'
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * Get the files that owns the Pages
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function files()
    {
        return $this->belongsTo('App\Models\Files');
    }
}
