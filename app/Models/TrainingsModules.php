<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrainingsModules extends Model
{
    use Sluggable, SoftDeletes;

    protected $table = 'trainings_modules';
    protected $fillable = [
        'active',
        'teams_id',
        'trainings_id',
        'title',
        'description',
        'start_at'
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * Get the teams that owns the TrainingsModules
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function teams(): BelongsTo
    {
        return $this->belongsTo(Teams::class);
    }

    /**
     * Get the trainings that owns the TrainingsModules
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function trainings(): BelongsTo
    {
        return $this->belongsTo(Trainings::class);
    }
}
