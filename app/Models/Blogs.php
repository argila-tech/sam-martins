<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blogs extends Model
{
    use Sluggable, SoftDeletes;

    protected $table = "blogs";
    protected $fillable = [
        'files_id',
        'users_id',
        'category_blogs_id',
        'title',
        'tags',
        'caption',
        'seo',
        'description',
        'publish_at',
        'active'
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function files()
    {
        return $this->belongsTo('App\Models\Files');
    }

    public function categorys()
    {
        return $this->belongsTo('App\Models\CategoryBlogs','category_blogs_id');
    }
}
