<?php

namespace App\Models;


use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Clients extends Model
{
    use Sluggable, SoftDeletes;

    protected $table = "clients";
    protected $fillable = [
        'files_id',
        'title',
        'active'
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * Get the files that owns the Clients
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function files()
    {
        return $this->belongsTo(Files::class);
    }
}
