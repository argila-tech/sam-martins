<?php

use App\Http\Controllers\PagesController as ControllerPagesController;
use App\Http\Controllers\dashboard\BlogsController;
use App\Http\Controllers\dashboard\ClientsController;
use App\Http\Controllers\dashboard\ContactsController;
use App\Http\Controllers\dashboard\PagesController;
use App\Http\Controllers\dashboard\PartnersController;
use App\Http\Controllers\dashboard\ProductsConttoller;
use App\Http\Controllers\dashboard\ServicesController;
use App\Http\Controllers\dashboard\SettingsConttoller;
use App\Http\Controllers\dashboard\SlidesController;
use App\Http\Controllers\dashboard\TeamsController;
use App\Http\Controllers\dashboard\TestimonialsController;
use App\Http\Controllers\dashboard\TrainigsSubscriptionsController;
use App\Http\Controllers\dashboard\TrainingsCategorys;
use App\Http\Controllers\dashboard\TrainingsController;
use App\Http\Controllers\dashboard\TrainingsModulesController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('',[ControllerPagesController::class ,'index'])->name('index');
Route::get('sobre',[ControllerPagesController::class ,'about'])->name('about');
Route::get('treinamentos',[ControllerPagesController::class ,'trainings'])->name('trainings');
Route::get('treinamento/{slug}',[ControllerPagesController::class ,'training'])->name('training');
// Route::get('eventos',[ControllerPagesController::class ,'events'])->name('events');
Route::get('depoimentos',[ControllerPagesController::class ,'testimonials'])->name('testimonials');
Route::get('clientes',[ControllerPagesController::class ,'clients'])->name('clients');
Route::get('parceiros',[ControllerPagesController::class ,'partners'])->name('partners');
Route::get('parceiro/{slug}',[ControllerPagesController::class ,'partner'])->name('partner');
Route::get('blog',[ControllerPagesController::class ,'blogs'])->name('blogs');
Route::get('blog/{slug}',[ControllerPagesController::class ,'blog'])->name('blog');
Route::get('servicos',[ControllerPagesController::class ,'services'])->name('services');
Route::get('servico/{slug}',[ControllerPagesController::class ,'service'])->name('service');
Route::get('contato/{slug?}',[ControllerPagesController::class ,'contacts'])->name('contacts');


Route::post('sendTrainings', [ControllerPagesController::class, 'sendTrainings'])->name('sendTrainings');
Route::post('sendContacts', [ControllerPagesController::class, 'sendContacts'])->name('sendContacts');

Route::get('sitemap', [ControllerPagesController::class, 'siteMap'])->name('siteMap');



Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['prefix' => 'dashboard' ,'as' => 'dashboard.','middleware' => 'auth'], function () {
	// Route::resource('user', 'App\Http\Controllers\dashboard\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\dashboard\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\dashboard\ProfileController@update']);
	Route::get('upgrade', function () {return view('pages.upgrade');})->name('upgrade'); 
	Route::get('map', function () {return view('pages.maps');})->name('map');
	Route::get('icons', function () {return view('pages.icons');})->name('icons'); 
	Route::get('table-list', function () {return view('pages.tables');})->name('table');
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\dashboard\ProfileController@password']);

	Route::resources([
		'trainings' 	=> TrainingsController::class,
		'trainings.modules'   => TrainingsModulesController::class,
		'categorys' 	=> TrainingsCategorys::class,
		'slides' 		=> SlidesController::class,
		'blog' 			=> BlogsController::class,
		'pages' 		=> PagesController::class,
		'settings' 		=> SettingsConttoller::class,
		'products' 		=> ProductsConttoller::class,
		'contacts' 		=> ContactsController::class,
		'clients' 		=> ClientsController::class,
		'teams' 		=> TeamsController::class,
		'testimonials' 	=> TestimonialsController::class,
		'subscriptions' => TrainigsSubscriptionsController::class,
		'services' 		=> ServicesController::class,
		'settings'		=> SettingsConttoller::class,
		'partners'		=> PartnersController::class
	]);
});

Route::fallback(function () {
    return abort(404, 'Página Não Encontrada.');
});
