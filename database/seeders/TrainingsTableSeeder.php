<?php

namespace Database\Seeders;

use App\Models\Files;
use App\Models\Trainings;
use App\Models\TrainingsCategorys;
use Illuminate\Database\Seeder;

class TrainingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = new Files;
        $file->name = 'slide.jpg';
        $file->filename = 'slide.jpg';
        $file->path = 'assets/img/curso.png';
        $file->save();

        foreach(TrainingsCategorys::all() as $item){
            $tra = new Trainings;
            $tra->files_id = $file->id;
            $tra->trainings_categorys_id = $item->id;
            $tra->active = true;
            $tra->title = 'YOUNG LEADER PROGRAM 2021 - PRESENCIAL';
            $tra->subtitle = 'Programa de Desenvolvimento em Liderança para Jovens';
            $tra->description = '<p>Desenvolver o conhecimento, compreensão e habilidades por meio das competências de liderança para torná-los jovens capazes de trabalhar em equipe, lidar com os desafios diários e assumir a posição de líder em seus respectivos Projetos.</p>
                    <br> <p><strong>O que está incluso no valor:</strong></p><p>Participação em todos os módulos;</p><p>Coffee Break nos encontros;</p><p>Certificado de participação.</p>';
            $tra->address = '<p><strong>Michelle Tour</strong></p><p>Rua Rui Barbosa, 911 - Tirol</p><p>CEP: 59015-290</p><p>Natal - RN</p>';
            $tra->map = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3969.3114752676834!2d-35.2035206846562!3d-5.81161299578883!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7b2ffefc33fce11%3A0x4915dc370855b120!2sMichelle%20Tour%20Ag%C3%AAncia%20de%20Viagens%20em%20Natal%2FRN!5e0!3m2!1spt-BR!2sbr!4v1630452261055!5m2!1spt-BR!2sbr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>';
            $tra->save(); 
        }
    }
}
