<?php

namespace Database\Seeders;

use App\Models\Clients;
use App\Models\Files;
use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i =1; $i <72; $i++){
            $file = new Files;
            $file->name = $i.'.png';
            $file->filename =  $i.'.png';
            $file->path = 'assets/img/clients/'. $i.'.png';
            $file->save();

            $cli = new Clients;
            $cli->title = 'Lorem Ipsum';
            $cli->active = true;
            $cli->files_id = $file->id;
            $cli->save();
        }
    }
}
