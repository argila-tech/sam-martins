<?php

namespace Database\Seeders;

use App\Models\Pages;
use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pages::create([
            'title' => 'Somos a San Matin',
            'description' => '<p>Ao longo de 21 anos atuando na área da gestão e educação corporativa, Fabio San Martin desenvolve trabalhos na área de consultoria, coaching, mentoring, assessorial, palestras e treinamentos para mais de 500 empresas, inclusive com atuação internacional.</p><p>A partir dessa experiência, identificaram que uma das principais dificuldades encontradas no ambiente organizacional tem origem na limitação das habilidades comportamentais dos profissionais, sejam líderes ou liderados.</p><br><p>Assim, o sucesso ou fracasso de uma organização depende do desenvolvimento dos profissionais: não apenas em relação aos aspectos técnicos, mas principalmente em relação aos aspectos comportamentais.</p><p>Como afirma Jim Collins, a maior autoridade do management atual, “é preciso ter as pessoas certas lugares certos e dar-lhes as condições necessárias para que alcancem grandes resultados”.</p><br><p>Mas, as instituições de ensino tradicionais e empresas de treinamentos estão mais direcionadas ao desenvolvimento das habilidades técnicas.</p><br><p><strong>Como mudar esse cenário?</strong></p><p>Entendendo a necessidade do mercado, foi criada, em dezembro de 2013, a San Martin &amp; Niklas Desenvolvimento Humano, que nasceu do sonho de transformar a vida de pessoas e empresas.</p><p>O nosso propósito é transformar a vida de empresas e pessoas. Para isso, dividimos nossa área de atuação em duas frentes: Desenvolvimento Humano e Desenvolvimento Organizacional.</p><br><p><strong>Desenvolvimento Humano</strong><br>Foca em desenvolver as pessoas, através do aprimoramento de suas respectivas competências, sejam elas técnicas ou comportamentais. Clique aqui e saiba mais sobre nossas soluções de Desenvolvimento Humano.</p><br><p><strong>Desenvolvimento Organizacional</strong><br>Foca em desenvolver as empresas, através do aprimoramento de suas equipes, através de técnicas de desenvolvimento ou repasse de know-how dos Sócios Diretores da empresa, contribuindo com a elevação da performance das pessoas e, consequentemente, da organização. Clique aqui e saiba mais sobre nossas soluções de Desenvolvimento Organizacional.</p><br><p>Se você quer transformação humana e organizacional, chegou ao lugar certo!</p><p>Com a San Martin &amp; Niklas, você vai potencializar performances e conquistar muito mais resultados.</p><p><br><strong>A EQUIPE SMN</strong></p><br><p>Com mais de duas décadas de experiência na área de Consultoria Empresarial, a equipe da San Martin &amp; Niklas acumula o know-how essencial para transformar a sua empresa. Além disso, todos os anos Fabio San Martin se aperfeiçoa, participando dos melhores cursos e evendos do management mundial, acessando o que há de mais importanto no context corporativo e organizacional. Já assistiram presencialmente grandes nomes e pensadores do management mundial, como Jim Collins, Ram Charam, Marshall Goldsmith, Steve Wozniac, Sthephen Covey, David Ulrich, Daniel Goleman, Amy Cuddy, William Ury, Alexander Osterwalder, Michael Porter, Adam Grant, Jonah Berger, James Hunter, Marcus Buckingham, Nassim Taleb, Robin Chase, Susan Cain, Tom Kelley, Martin Lindstron, Robert Cialdini, dentre vários outros.</p>'
        ]);
    }
}
