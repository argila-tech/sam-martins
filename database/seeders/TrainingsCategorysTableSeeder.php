<?php

namespace Database\Seeders;

use App\Models\TrainingsCategorys;
use Illuminate\Database\Seeder;

class TrainingsCategorysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'title' => 'In Company',
                'active' => true
            ],
            [
                'title' => 'On Line',
                'active' => true
            ],
            [
                'title' => 'Programas de Desenvolvimento',
                'active' => true
            ],
        ];

        foreach($items as $item){
            TrainingsCategorys::create($item);
        }
    }
}
