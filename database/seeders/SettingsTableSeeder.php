<?php

namespace Database\Seeders;

use App\Models\Settings;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'title' => 'Email contato',
                'description' => "diretoria@fabiosanmartin.com.br",
            ],
            [
                'title' => 'Facebook',
                'description' => '',
            ],
            [
                'title' => 'Instagram',
                'description' => 'https://www.instagram.com/portalsmn/',
            ],
            [
                'title' => 'YouTube',
                'description' => 'https://youtube.com',
            ],
            [
                'title' => 'Linkedin',
                'description' => 'https://www.linkedin.com/company/sanmartininteligencia/',
            ],
            [
                'title' => 'Whatsapp',
                'description' => "",
            ],
            [
                'title' => 'Contato',
                'description' => "(84) 99925-3746",
            ],
            [
                'title' => 'Google Analytics Head',
                'description' => '  '
            ],
            [
                'title' => 'Google Analytics Boby',
                'description' => '  '
            ],
            [
                'title' => 'Descricao do site',
                'description' => 'San Martin Desenvolvimento Humano e Organizacional | Treinamentos, Palestras, Workshops, Coaching, Programas de Capacitação e Assessoria Empresarial'
            ],
            [
                'title' => 'Palavras Chaves',
                'description' => 'Treinamentos, Palestras, Workshops, Coaching, Programas de Capacitação e Assessoria Empresarial'
            ],
        ];

        foreach ($items as $value) {
            Settings::create($value);
        }
    }
}
