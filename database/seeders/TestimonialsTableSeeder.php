<?php

namespace Database\Seeders;

use App\Models\Files;
use App\Models\Testimonials;
use Illuminate\Database\Seeder;

class TestimonialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = new Files;
        $file->name = 'slide.jpg';
        $file->filename = 'slide.jpg';
        $file->path = 'assets/img/curso.png';
        $file->save();

        for ($i=0; $i < 10; $i++) {
            $t = new Testimonials;
            $t->files_id = $file->id;
            $t->title = 'Felipe Mendonca';
            $t->office = 'CTO Argila Tech';
            $t->description = 'Lorem ipsum dolor sit amet, consectetur adip iscing elit sed. orci urna. In et augue ornare, tempor massa in, luctus sapien. Proin a diam et dui fermentum dolor molestie vel id neque euismod massa a quam viverra et.';
            $t->save();
        }
    }
}
