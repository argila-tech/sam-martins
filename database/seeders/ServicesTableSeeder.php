<?php

namespace Database\Seeders;

use App\Models\Services;
use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'title' => 'Análise Comportamental Disc',
                'description' => 'O Teste DISC avalia o comportamento das pessoas em um certo ambiente e é baseado na teoria desenvolvida pelo psicólogo William Moulton Marston – ninguém menos que o criador da Mulher Maravilha. A avaliação leva esse nome por afirmar que existem quatro traços básicos de comportamento nas pessoas: Dominância (D), Influência (I), Estabilidade (S) e Complacência (C). Vale lembrar que não somos 100% um só estilo. Na verdade, costumamos ter um ou dois estilos que predominam, e nenhum é melhor que o outro.',
                'icon' => '<i class="fas fa-chart-bar"></i>'
            ],
            [
                'title' => 'Assessoria em Gestão',
                'description' => 'A Assessoria em Gestão tem por finalidade oferecer aos gestores clientes know-how e expertise técnica em gestão, para que possam ser tomadas decisões e realizadas implementações no âmbito organizacional. ',
                'icon' => '<i class="fas fa-project-diagram"></i>'
            ],
            [
                'title' => 'Atividade de Integração',
                'description' => 'Participação em eventos corporativos e desenvolvimento de dinâmicas motivacionais e interativas.',
                'icon' => '<i class="fas fa-users"></i>'
            ],
            [
                'title' => 'Execute Coaching - Coaching Pessoal e Profissionação',
                'description' => 'É um Processo de desenvolvimento humano em que um facilitador (Coach) ajuda seu cliente (Coachee) a se responsabilizar por sua vida e a encontrar as próprias respostas para desenvolver as habilidades e competências necessárias ao alcance de seus objetivos, com foco na realização de ações coerentes com a meta e o propósito do Cliente.',
                'icon' => '<i class="fas fa-thumbs-up"></i>'
            ],
            [
                'title' => 'Mentoring Empresarial',
                'description' => 'O "Mentoring" ou "Mentoria", diz respeito a uma relação pessoal de desenvolvimento, em que uma das pessoas - a mais experiente - promove a evolução e desenvolvimento da pessoa menos experiente. O objetivo principal é motivar e inspirar o "mentorado", aumentar o seu potencial, para que ele melhore sua performance. As sessões de mentoria podem ser presenciais ou on line, através de video conferência.',
                'icon' => '<i class="fas fa-handshake"></i>'
            ],
            [
                'title' => 'Palestras e Workshops',
                'description' => 'As palestras e workshops sempre são desenvolvidas entendendo a necessidade do cliente. Ou seja, essas atividades sempre são personalizadas, para que o evento do cliente possa alcançar o resultado esperado. Um dos diferenciais das palestras e workshops da San Martin é que eles podem acontecer de forma presencial ou através de video conferência (Lives), otimizando dessa forma os resultados do cliente.',
                'icon' => '<i class="fas fa-gifts"></i>'
            ],
            [
                'title' => 'Programas de Desenvolvimento e Capacitação',
                'description' => 'A San Martin & Niklas é referencia no desenvolvimento de líderes. Somos a única empresa do RN a oferecer Programas de Liderança segmentados: PDL Alta Gestão, voltado para gestores e empresários; o PDL Middle Management, voltados para os líderes da média gestão, como gerentes, coordenadores e supervisores; O Young Leader Program, voltados para jovens universitários, que pretendem desenvolver competências de Liderança; O Masterclass, voltado para Lideres da Alta gestão que já participaram do PDL Alta Gestão. Além de capacitar em Liderança, a SMN também oferece um Programa voltado para pessoas que desejam melhorar sua performance em atendimento e vendas, através do Programa de Desenvolvimento em Atendimento e Vendas – PDAV. Todos eles Programas abertos, com a participação de profissionais de várias empresas. Além dos programas abertos, a SMN também desenvolve Programas in company, que são elaborados de forma personalizada para empresas públicas e privadas. Tudo sempre com um grande diferencial: uma metodologia própria, que está contribuindo com a elevação dos resultados das empresas clientes.',
                'icon' => '<i class="fas fa-book"></i>'
            ],
            [
                'title' => 'Recrutamento e Seleção de Líderes - Head Hunter',
                'description' => 'Com uma metodologia Inovadora, a San Martin & Niklas é especialista em identificar no mercado talentos e selecionar os melhores profissionais para a sua Empresa, diminuindo em 90% a probabilidade de contratar a pessoa errada. A equipe de profissionais da Empresa é especialista em análise comportamental na metodologia DISC, pela Etalent Brasil, que é a Empresa líder do segmento na América Latina.',
                'icon' => '<i class="fas fa-hand-holding"></i>'
            ],
            [
                'title' => 'Treinamentos',
                'description' => 'Exposição didática - presencial e on line - de conhecimentos diversos relacionados à diversos temas da gestão, como liderança, atendimento, vendas, com o objetivo de desenvolvimento de habilidades técnicas e comportamentais.',
                'icon' => '<i class="fas fa-hands-helping"></i>'
            ]
        ];


        foreach($items as $item){
            Services::create($item);
        }
    }
}
