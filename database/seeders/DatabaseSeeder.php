<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            ServicesTableSeeder::class,
            TeamsTableSeeder::class,
            SlidesTableSeeder::class,
            TestimonialsTableSeeder::class,
            CategoryBlogsTableSeeder::class,
            ClientsTableSeeder::class,
            PagesTableSeeder::class,
            SettingsTableSeeder::class,
            TrainingsCategorysTableSeeder::class,
            // TrainingsTableSeeder::class,
        ]);
    }
}
