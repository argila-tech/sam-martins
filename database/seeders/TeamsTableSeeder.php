<?php

namespace Database\Seeders;

use App\Models\Teams;
use App\Models\Files;
use Illuminate\Database\Seeder;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = new Files;
        $file->name = 'slide.jpg';
        $file->filename = 'slide.jpg';
        $file->path = 'assets/img/slide.jpg';
        $file->save();

        $team = new Teams;
        $team->files_id = $file->id;
        $team->title = 'Fabio San Martin';
        $team->description = 'Formado em Administração de empresas; Possui MBA em Gestão de Negócios pelo Ibmec; É Líder Coach pelo Integrated Coach Institute – ICI/USA. Possui formação em sistema de qualidade ISO 9.000.

15 anos de experiência e 160 empresas clientes; Experiência internacional: Estados Unidos (2001) e Angola (2011 a 2013); Professor convidado da Pós-graduação da Universidade Potiguar (UnP) desde 2008.

Desenvolveu trabalhos para o Restaurante Camarões (eleito um dos 5 melhores do país em 2013), Queiroz Galvão Angola (3ª maior empresa brasileira com atividades em Angola), Prosegur (3ª maior empresa de segurança do mundo), entre outras.

É Sócio-diretor da San Martin & Niklas Desenvolvimento Humano e Organizacional. Desenvolve Assessoria, Coaching e Programas de Capacitação para a RedeMAIS, Famiglia Reis Magos, Praia Shopping, Cyrela Plano&Plano, DNA Center e para a Aliança Center, além de desenvolver Programas deCapacitação com a presença de diversas empresas.';
        $team->save();
    }
}
