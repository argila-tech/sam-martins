<?php

namespace Database\Seeders;

use App\Models\CategoryBlogs;
use Illuminate\Database\Seeder;

class CategoryBlogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['title' => 'Administração'],
            ['title' => 'Carreira'],
            ['title' => 'Clientes e Vendas'],
            ['title' => 'Economia'],
            ['title' => 'Finanças'],
            ['title' => 'Gestão'],
            ['title' => 'Inspiração'],
            ['title' => 'Liderança'],
        ];

        foreach($items as $item){
            CategoryBlogs::create($item);
        }
    }
}
