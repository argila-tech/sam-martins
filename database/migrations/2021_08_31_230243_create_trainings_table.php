<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainings', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('trainings_categorys_id')->nullable();
            $table->foreign('trainings_categorys_id')->references('id')->on('trainings_categorys')->onDelete('cascade');
            $table->uuid('files_id');
            $table->foreign('files_id')->references('id')->on('files')->onDelete('cascade');
            $table->boolean('active');
            $table->string('title');
            $table->string('subtitle')->nullable();
            $table->float('price')->nullable();
            $table->string('installments')->nullable();
            $table->date('init_at')->nullable();
            $table->date('finish_at')->nullable();
            $table->text('next')->nullable();
            $table->text('tags')->nullable();
            $table->text('description')->nullable();
            $table->text('address')->nullable();
            $table->text('map')->nullable();
            $table->string('slug');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainings');
    }
}
