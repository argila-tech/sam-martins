<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingsSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainings_subscriptions', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->enum('read',['0','1'])->default('0');
            $table->uuid('trainings_id');
            $table->foreign('trainings_id')->references('id')->on('trainings')->onDelete('cascade');
            $table->string('name');
            $table->string('phone');
            $table->string('email');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainings_subscriptions');
    }
}
