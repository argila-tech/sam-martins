<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingsModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainings_modules', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('teams_id');
            $table->foreign('teams_id')->references('id')->on('teams')->onDelete('cascade');
            $table->uuid('trainings_id');
            $table->foreign('trainings_id')->references('id')->on('trainings')->onDelete('cascade');
            $table->boolean('active');
            $table->string('title');
            $table->text('description');
            $table->string('slug');
            $table->date('start_at');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainings_modules');
    }
}
